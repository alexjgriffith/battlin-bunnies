#pragma language glsl1
// varying vec4 vpos;

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    // The order of operations matters when doing matrix multiplication.
    // vpos = vertex_position;
    return transform_projection * vertex_position;
}
#endif

#ifdef PIXEL
uniform Image mask;
uniform vec2 maskSize;
uniform bool onField;
vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
  vec4 maskcolor = Texel(mask, screen_coords / maskSize );
    
    vec4 texturecolor = Texel(tex, texture_coords);
    vec4 texturecolor2 = texturecolor;
    if (maskcolor.g > 0.0 && ! onField){
      texturecolor=vec4(0.0,0.0,0.0,0.0);
    }
    else if (maskcolor.g == 0.0 && onField){
      texturecolor=vec4(0.0,0.0,0.0,0.0);
    }
    if(maskcolor.b == 1.0){
      texturecolor =  vec4(0.0,0.0,0.0,0.0);
    }
    return texturecolor * color;
}
#endif
