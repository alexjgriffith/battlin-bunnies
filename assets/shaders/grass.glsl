#pragma language glsl1

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    // The order of operations matters when doing matrix multiplication.
    // vpos = vertex_position;
    return transform_projection * vertex_position;
}
#endif

#ifdef PIXEL
uniform vec2 camera;
uniform Image mask;
uniform vec2 maskSize;
uniform vec2 atlasSize;
uniform float scale;

uniform vec2 spriteAtlasPosition;
uniform vec2 spriteSize;
uniform float depth;
uniform float above;

vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
    float quad_coords_y = (atlasSize.y / spriteSize.y) * (texture_coords.y - (spriteAtlasPosition.y / atlasSize.y));
    float quad_coords_x = (atlasSize.x / spriteSize.x) * (texture_coords.x - (spriteAtlasPosition.x / atlasSize.x));
    float h = spriteSize.y;
    float w = spriteSize.x;
    float y = floor(quad_coords_y * spriteSize.y);
    float x = floor(quad_coords_x * spriteSize.x);
    
    vec4 maskcolor = Texel(mask, screen_coords/ maskSize);
    
    vec4 texturecolor = Texel(tex, texture_coords);
    float origa = texturecolor.a;
    const float norm = 20.0 / 256.0;
    float d = depth; // depth
    if (// y > (h - d) && maskcolor.g == 1.0 //||
        // y == (h - d) && maskcolor.g >= 16.0 * norm ||
        y == (h - d + 0.0) && maskcolor.g >= 20.0 * norm ||
        y == (h - d + 1.0) && maskcolor.g >= 19.0 * norm ||
        y == (h - d + 2.0) && maskcolor.g >= 18.0 * norm ||
        y == (h - d + 3.0) && maskcolor.g >= 17.0 * norm ||
        y == (h - d + 4.0) && maskcolor.g >= 16.0 * norm ||
        y == (h - d + 5.0) && maskcolor.g >= 15.0 * norm ||
        y == (h - d + 6.0) && maskcolor.g >= 14.0 * norm ||
        y == (h - d + 7.0) && maskcolor.g >= 13.0 * norm ||
        y == (h - d + 8.0) && maskcolor.g >= 12.0 * norm ||
        y == (h - d + 9.0) && maskcolor.g >= 11.0 * norm ||
        y == (h - d + 10.0) && maskcolor.g >= 10.0 * norm ||
        y == (h - d + 11.0) && maskcolor.g >= 9.0 * norm ||
        y == (h - d + 12.0) && maskcolor.g >= 8.0 * norm ||
        y == (h - d + 13.0) && maskcolor.g >= 7.0 * norm ||
        y == (h - d + 14.0) && maskcolor.g >= 6.0 * norm ||
        y == (h - d + 15.0) && maskcolor.g >= 5.0 * norm ||
        y == (h - d + 16.0) && maskcolor.g >= 4.0 * norm ||
        y == (h - d + 17.0) && maskcolor.g >= 3.0 * norm ||
        y == (h - d + 18.0) && maskcolor.g >= 2.0 * norm ||
        y == (h - d + 19.0) && maskcolor.g >= 1.0 * norm
        ){
      texturecolor=vec4(0.0,0.0,0.0,0.0);
    }
    if ((y == (h - d + 8.0) && maskcolor.g >= 10.0 * norm ||
        y == (h - d + 9.0) && maskcolor.g >= 10.0 * norm) &&
        origa > 0.8
        ){
      texturecolor=vec4(0.0,0.0,0.0,1.0);
    }
    
    return texturecolor * color;
}
#endif
