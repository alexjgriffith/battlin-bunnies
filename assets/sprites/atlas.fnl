{:size {:w 2680 :h 1560}
 :tilewidth 64
 :slices
 {;; Tiles
  :TallGrass   {:bounds {:scale :tile :x 1 :y 1  :w 4 :h 4}}
  :Cliff       {:bounds {:scale :tile :x 6 :y 1  :w 4 :h 4}}
  :Road        {:bounds {:scale :tile :x 1 :y 6  :w 4 :h 4}}
  :Water       {:bounds {:scale :tile :x 6 :y 6  :w 4 :h 4}}
  :Ground      {:bounds {:scale :tile :x 1 :y 4  :w 1 :h 1}}

  :TallGrassMask   {:bounds {:scale :tile :x 25 :y 1  :w 4 :h 4}}
  :CliffMask       {:bounds {:scale :tile :x 30 :y 1  :w 4 :h 4}}
  :WaterMask       {:bounds {:scale :tile :x 25 :y 6  :w 4 :h 4}}

  ;; Objects
  :SmallTree   {:bounds {:scale :tile :x 1 :y 11  :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16}}
  
  :LargeTree   {:bounds {:scale :tile :x 2 :y 11  :w 2 :h 2}
                :data {:cx 48 :cy 112 :cw 32 :ch 16}}
  
  :Stump       {:bounds {:scale :tile :x 4 :y 12 :w 1 :h 1}
                :data {:cx 12 :cy 48 :cw 40 :ch 16}}
  
  :Rock1       {:bounds {:scale :tile :x 5 :y 12 :w 1 :h 1}
                :data {:cx 12 :cy 48 :cw 40 :ch 16}}
  
  :Rock2       {:bounds {:scale :tile :x 6 :y 12 :w 1 :h 1}
                :data {:cx 12 :cy 48 :cw 40 :ch 16}}
  
  :FenceLeft   {:bounds {:scale :tile :x 7 :y 12 :w 1 :h 1}
                :data {:cx 48 :cy 48 :cw 16 :ch 16}}
  
  :FenceCenter {:bounds {:scale :tile :x 8 :y 12 :w 1 :h 1}
                :data {:cx 0 :cy 48 :cw 64 :ch 16}}
  
  :FenceRight  {:bounds {:scale :tile :x 9 :y 12 :w 1 :h 1}
                :data {:cx 0 :cy 48 :cw 16 :ch 16}}
  ;; Characters
  :Tim         {:bounds {:scale :tile :x 12 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}
  :Sara        {:bounds {:scale :tile :x 13 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}
  :Mike        {:bounds {:scale :tile :x 14 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}
  :Alan        {:bounds {:scale :tile :x 15 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}
  :Ben         {:bounds {:scale :tile :x 16 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}
  :Kat         {:bounds {:scale :tile :x 17 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}
  :Will        {:bounds {:scale :tile :x 18 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}
  :Zach        {:bounds {:scale :tile :x 19 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}
  :Becca       {:bounds {:scale :tile :x 20 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}
  :Nora        {:bounds {:scale :tile :x 21 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}

  ;; Player
  :Player      {:bounds {:scale :tile :x 22 :y 1 :w 1 :h 2}
                :data   {:cx 24 :cy 112 :cw 16 :ch 16
                         :shadow :Shadow :sy (+ 64 16) :sx 0}}

  :PlayerCrouch      {:bounds {:scale :tile :x 23 :y 1 :w 1 :h 2}}
  
  ;; Bunnies
  :Chonkers         {:bounds {:scale :tile :x 12 :y 4 :w 1 :h 1}
                     :data   {:cx (- 30 0) :cy (- 30 0) :cw 6 :ch 6
                              :shadow :Shadow :sy  16 :sx 0}}
  :Leggy        {:bounds {:scale :tile :x 13 :y 4 :w 1 :h 1}
                 :data   {:cx (- 30 0) :cy (- 30 0) :cw 6 :ch 6
                          :shadow :Shadow :sy  16 :sx 0}}
  :Square        {:bounds {:scale :tile :x 14 :y 4 :w 1 :h 1}
                  :data   {:cx (- 30 0) :cy (- 30 0) :cw 6 :ch 6
                           :shadow :Shadow :sy  16 :sx 0}}
  :Ploof        {:bounds {:scale :tile :x 15 :y 4 :w 1 :h 1}
                 :data   {:cx (- 30 0) :cy (- 30 0) :cw 6 :ch 6
                          :shadow :Shadow :sy  16 :sx 0}}
  :Loaf         {:bounds {:scale :tile :x 16 :y 4 :w 1 :h 1}
                 :data   {:cx (- 30 0) :cy (- 30 0) :cw 6 :ch 6
                          :shadow :Shadow :sy  16 :sx 0}}
  :Upright         {:bounds {:scale :tile :x 17 :y 4 :w 1 :h 1}
                    :data   {:cx (- 30 0) :cy (- 30 0) :cw 6 :ch 6
                             :shadow :Shadow :sy  16 :sx 0}}
  :LilEar        {:bounds {:scale :tile :x 18 :y 4 :w 1 :h 1}
                  :data   {:cx (- 30 0) :cy (- 30 0) :cw 6 :ch 6
                           :shadow :Shadow :sy  16 :sx 0}}
  :BigEar        {:bounds {:scale :tile :x 19 :y 4 :w 1 :h 1}
                  :data   {:cx (- 30 0) :cy (- 30 0) :cw 6 :ch 6
                           :shadow :Shadow :sy  16 :sx 0}}
  :Drowsy       {:bounds {:scale :tile :x 20 :y 4 :w 1 :h 1}
                 :data   {:cx (- 30 0) :cy (- 30 0) :cw 6 :ch 6
                          :shadow :Shadow :sy  16 :sx 0}}
  :Fury        {:bounds {:scale :tile :x 21 :y 4 :w 1 :h 1}
                :data   {:cx (- 30 0) :cy (- 30 0) :cw 6 :ch 6
                         :shadow :Shadow :sy  16 :sx 0}}
  
  :ChonkersOutline {:bounds {:scale :tile :x 12 :y 5 :w 1 :h 1}}
  :LeggyOutline {:bounds {:scale :tile :x 13 :y 5 :w 1 :h 1}}
  :SquareOutline {:bounds {:scale :tile :x 14 :y 5 :w 1 :h 1}}
  :PloofOutline {:bounds {:scale :tile :x 15 :y 5 :w 1 :h 1}}
  :LoafOutline {:bounds {:scale :tile :x 16 :y 5 :w 1 :h 1}}
  :UprightOutline {:bounds {:scale :tile :x 17 :y 5 :w 1 :h 1}}
  :LilEarOutline {:bounds {:scale :tile :x 18 :y 5 :w 1 :h 1}}
  :BigEarOutline {:bounds {:scale :tile :x 19 :y 5 :w 1 :h 1}}
  :DrowsyOutline {:bounds {:scale :tile :x 20 :y 5 :w 1 :h 1}}
  :FuryOutline {:bounds {:scale :tile :x 21 :y 5 :w 1 :h 1}}
  

  :BigTree {:bounds {:h 6 :scale "tile" :w 6 :x 0 :y 13}
            :data {:ch 22 :cw 96 :cx (+ 64 72) :cy (+ 64 296)
                   :shadow :BigTreeShadow :sy (+ 32 64 (* 64 3))  :sx (+ 24 64) }}
  :UFence {:bounds {:h 1 :scale "tile" :w 1 :x 10 :y 14} :data {}}
  :UFenceDown {:bounds {:h 1 :scale "tile" :w 1 :x 10 :y 15} :data {}}
  :UFenceUp {:bounds {:h 1 :scale "tile" :w 1.015625 :x 10 :y 13} :data {}}  
  :Hole {:bounds {:h 1 :scale "tile" :w 1 :x 12 :y 12} :data {}}
  
  :BigBunny {:bounds {:h 5 :scale "tile" :w 5 :x 13 :y 6} :data {}}
  :BigExclaim {:bounds {:h 3 :scale "tile" :w 2 :x 18 :y 8} :data {}}

  :BlockDown {:bounds {:h 3 :scale "tile" :w 1 :x 7 :y 14} :data {}}
  :BlockUp {:bounds {:h 3 :scale "tile" :w 1 :x 6 :y 14} :data {}}
  :Wall {:bounds {:h 3 :scale "tile" :w 1 :x 6 :y 14}
         :data {:cx -16 :cy 0 :cw (+ 32 (* 64 1)) :ch (* 64 3)}}
  
  :Bone {:bounds {:h 1 :scale "tile" :w 1 :x 5 :y 11} :data {}}
  :Exclaim {:bounds {:h 1 :scale "tile" :w 1 :x 12 :y 13} :data {}}
  :Question {:bounds {:h 1 :scale "tile" :w 1 :x 12 :y 14} :data {}}
  :Rock {:bounds {:h 1 :scale "tile" :w 1 :x 6 :y 11} :data {}}
  :Smile {:bounds {:h 1 :scale "tile" :w 1 :x 12 :y 15} :data {}}
  :SquareBlock {:bounds {:h 2 :scale "tile" :w 2 :x 8 :y 14} :data {}}

  :SmallBall {:bounds {:h 0.5 :scale "tile" :w 0.5 :x 7 :y 11} :data {}}
  :SmallBone {:bounds {:h 0.5 :scale "tile" :w 0.5 :x 7.5 :y 11} :data {}}
  :SmallRock {:bounds {:h 0.5 :scale "tile" :w 0.5 :x 7 :y 11.5}
             :data {}}

 :FenceShadow {:bounds {:h 1 :scale "tile" :w 1 :x 7 :y 12} :data {}}
 :FenceShadowLeft {:bounds {:h 1 :scale "tile" :w 1 :x 6 :y 12} :data {}}
 :FenceShadowRight {:bounds {:h 1 :scale "tile" :w 1 :x 8 :y 12} :data {}}  
 :BigTreeShadow {:bounds {:h 2 :scale "tile" :w 3 :x 13 :y 13} :data {}}  
 :Shadow {:bounds {:h 1 :scale "tile" :w 1 :x 13 :y 12} :data {}}
 :UFenceDownShadow {:bounds {:h 1 :scale "tile" :w 1 :x 10 :y 14} :data {}}
 :UFenceShadow {:bounds {:h 1 :scale "tile" :w 1.015625 :x 10 :y 13} :data {}}
 :UFenceUpShadow {:bounds {:h 1 :scale "tile" :w 1 :x 10 :y 12} :data {}}  
  
  ;; :BigTree {:bounds {:scale :pixel :h 320 :w 320 :x 0 :y 832}
  ;;           :data {:cx 72 :cy 296 :cw 96 :ch 22
  ;;                  ;; :shadow "BigTreeShadow"
  ;;                  ;; :sy (+ 32 (* 64 3))
  ;;                  ;; :sx 32
  ;;                  }}
  ;; :Hole {:bounds {:scale :pixel :h 64 :w 64 :x 704 :y 704} :data {}}
  ;; :UFence {:bounds {:scale :pixel :h 64 :w 64 :x 576 :y 832} :data {}}
  ;; :UFenceDown {:bounds {:scale :pixel :h 64 :w 64 :x 576 :y 896} :data {}}
  ;; :UFenceUp {:bounds {:scale :pixel :h 64 :w 65 :x 576 :y 768} :data {}}
  
  ;; :BigBunny {:bounds {:scale :pixel :h 320 :w 320 :x 768 :y 320} :data {}}
  ;; :BigExclaim {:bounds {:scale :pixel :h 192 :w 128 :x 1088 :y 448} :data {}}
  ;; :BlockDown {:bounds {:scale :pixel :h 192 :w 64 :x 384 :y 832} :data {}}
  ;; :BlockUp {:bounds {:scale :pixel :h 192 :w 64 :x 320 :y 832} :data {}}
  ;; :Bone {:bounds {:scale :pixel :h 64 :w 64 :x 256 :y 640} :data {}}
  ;; :Exclaim {:bounds {:scale :pixel :h 64 :w 64 :x 704 :y 768} :data {}}
  ;; :Question {:bounds {:scale :pixel :h 64 :w 64 :x 704 :y 832} :data {}}
  ;; :Rock {:bounds {:scale :pixel :h 64 :w 64 :x 320 :y 640} :data {}}
  ;; :SmallBall {:bounds {:scale :pixel :h 32 :w 32 :x 384 :y 640} :data {}}
  ;; :SmallBone {:bounds {:scale :pixel :h 32 :w 32 :x 416 :y 640} :data {}}
  ;; :SmallRock {:bounds {:scale :pixel :h 32 :w 32 :x 385 :y 672} :data {}}
  ;; :Smile {:bounds {:scale :pixel :h 64 :w 64 :x 704 :y 896} :data {}}
  ;; :Square {:bounds {:scale :pixel :h 128 :w 128 :x 448 :y 832} :data {}}

  ;; :Shadow {:bounds {:scale :pixel :h 64 :w 64 :x 768 :y 704} :data {}}  
  ;; :BigTreeShadow {:bounds {:scale :pixel :h 128 :w 192 :x 768 :y 768} :data {}}  
  ;; :FenceShadow {:bounds {:scale :pixel :h 64 :w 64 :x 448 :y 768} :data {}}
  ;; :FenceShadowLeft {:bounds {:scale :pixel :h 64 :w 64 :x 384 :y 768} :data {}}
  ;; :FenceShadowRight {:bounds {:scale :pixel :h 64 :w 64 :x 512 :y 768} :data {}}  
  ;; :UFenceDownShadow {:bounds {:scale :pixel :h 64 :w 64 :x 640 :y 896} :data {}}
  ;; :UFenceShadow {:bounds {:scale :pixel :h 64 :w 65 :x 640 :y 832} :data {}}
  ;; :UFenceUpShadow {:bounds {:scale :pixel :h 64 :w 64 :x 640 :y 768} :data {}}
  }
 }
