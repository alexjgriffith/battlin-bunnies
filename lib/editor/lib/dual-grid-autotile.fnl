
(fn tile [base-tile TL TR BL BR]
  (var err nil)
  (local index-value (if (~= TL base-tile) TL
                           (~= TR base-tile) TR
                           (~= BL base-tile) BL
                           (~= BR base-tile) BR
                           base-tile))
  (when (not (and (or (= index-value base-tile) (= TL index-value))
                  (or (= index-value base-tile) (= TR index-value))
                  (or (= index-value base-tile) (= BL index-value))
                  (or (= index-value base-tile) (= BR index-value))))
    (set err :invalid-neighbour-tile))
  
  (local quad-index
         (if (= index-value base-tile) 1
             (do
               (var quad-index 1)
               (when (= index-value BR) (+ quad-index (^ 2 0)))
               (when (= index-value Bl) (+ quad-index (^ 2 1)))
               (when (= index-value TR) (+ quad-index (^ 2 2)))
               (when (= index-value Tl) (+ quad-index (^ 2 3)))
               quad-index)))
  (values (= err nil) (if (= err nil) index-value err) quad-index))

(fn make-grid [world-grid-array base-tile grid-width]
  "Takes an flattened nxm grid as world-grid-array and returns
an array n+1xm+1 as autotile-grid-array.
base-tile: The name of the base tile
grid-width: how wide is the grid

example:
(autotile-grid [0 0 0
;;              0 1 0
;;              0 0 0] 0 3)
;; > [0 1  0 1  0 1  0 1
;;    0 1  1 5  1 9  0 1
;;    0 1  1 2  1 3  0 1
;;    0 1  0 1  0 1  0 1]
"
  (let [autotile-grid-array []
        w (+ grid-width 1)
        height (math.floor (/ (# world-grid-array grid-width)))
        h (+ height 1)]
    (var err nil)
    (for [r 0 (- h 1)]
      (for [c 1 w]
        (let [TL (. autotile-grid-array (+ (+ c -1) (* (+ r -1) w)))
              TR (. autotile-grid-array (+ (+ c 0) (* (+ r -1) w)))
              BL (. autotile-grid-array (+ (+ c -1) (* (+ r 0) w)))
              BR (. autotile-grid-array (+ (+ c 0) (* (+ r 0) w)))]
          (local (success index-value quad-index) (tile base-tile TL TR BL BR))
          (if success
              (do (table.insert autotile-grid-array index-value)
                  (table.insert autotile-grid-array quad-index))
              (set err index-value)))))
    (values (= err nil)  (if (= err nil) autotile-grid-array err))))

(fn update-xy! [autotile-grid-array world-grid-array base-tile grid-width x y value collection? callback?]
  ;; update the 4 tiles impacted by changing x y
  ;; if a collection has been made from the autotile that can also be updated
  (let [index-world ()
        tile-1 
        tile-2
        tile-3
        tile-4 ]
    ;; set the world-grid-array to at x y to be value

    ;; update the 4 impacted fields in autotile-grid-array
    (local (success index-value quad-index) (tile base-tile TL TR BL BR))

    ;; update the collection if pressent
    (values success autotile-grid-array world-grid-array collection?)
    ))

(fn alternate-quads! [autotile-grid-array quad-map]
  (for [i 1 (# autotile-grid-array) 2]
    (let [initial (. autotile-grid-array (+ i 1))
          new (. quad-map initial)]
      (tset autotile-grid-array (+ 1 i) new)))
  (values true autotile-grid-array))

(fn map [autotile-grid-array callback]
  (let [collection []]
    (for [i 1 (# autotile-grid-array) 2]
      (table.insert collection (callback (. autotile-grid-array i) (. autotile-grid-array (+ i 1)))))
    (values true collection)))

{: make-grid
 : update-xy
 : alternate-quads
 : map}
