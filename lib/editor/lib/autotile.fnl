(local autotile {})

(fn autotile.set-index [self i w ...]
  (local xi (+ 1 (% (- i 1) w)))
  (local yi (+ 1 (math.floor (/ (- i 1) w))))
  (local t {:x xi :y yi})
  (local args [...])
  (local l (length args))
  (for [i 1 l 2]
    (tset t (. args i) (. args (+ i 1))))  
  (tset self i t)
  self)

(fn autotile.set [self x y w ...]
  (local i (+ 1 (* w (- y 1)) (- x 1)))
  (local t {:x x :y y})
  (local args [...])
  (local l (length args))
  (for [i 1 l 2]
    (tset t (. args i) (. args (+ i 1))))  
  (tset self i t)
  self)

(fn autotile.set-tile [self x y w char brush ?bitmap-index ?ignore]
  (autotile.set self x y w
                :bitmap-index (or ?bitmap-index "NA")
                :char char
                :brush brush
                :ignore (or ?ignore "NA")))

(tset autotile :bitmap-3x3-simple
"
000 000 000 000
010 011 111 110
010 01_ _1_ _10

010 01_ _1_ _10
010 011 111 110
010 01_ _1_ _10

010 01_ _1_ _10
010 011 111 110
000 000 000 000

000 000 000 000
010 011 111 110
000 000 000 000
")

(tset autotile :bitmap-3x3-simple-alt
"
_0_ _0_ _0_ _0_
010 011 111 110
_1_ _1_ _1_ _1_

_1_ _1_ _1_ _1_
010 011 111 110
_1_ _1_ _1_ _1_

_1_ _1_ _1_ _1_
010 011 111 110
_0_ _0_ _0_ _0_

_0_ _0_ _0_ _0_
010 011 111 110
_0_ _0_ _0_ _0_
")

(tset autotile :bitmap-2x2
"
_0_ _11 110 _0_
110 011 111 111
11_ _11 111 111

110 011 1_1 111
111 111 _1_ 111
011 111 1_1 110

_11 111 111 11_
011 111 111 110
_0_ _0_ 011 11_

000 _0_ 011 11_
010 011 111 110
000 _11 110 _0_
")

(tset autotile :bitmap-1
"
___
_1_
___
")

(fn n [t i w d]
  (fn check [t i oy ox d]
    (local row (math.floor (/ (+ i oy -1) w)))
    (local index (+ i oy ox))
    (local index-row (math.floor (/ (- index 1) w)))
    (if (and (= index-row row) (. t index))
        (. t index)
        d))
  [(check t i (- w) -1 d) ;; tl
   (check t i (- w) 0  d) ;; t
   (check t i (- w) 1  d) ;; tr
   (check t i 0 1 d)      ;; r
   (check t i w 1 d)      ;; br
   (check t i w 0  d)     ;; b
   (check t i w -1 d)     ;; bl
   (check t i 0 -1 d)])   ;; l

(fn f-n [t i w d]  
  (let [r [(. t i)]
        t2 (n t i w d)]
    (each [k v (ipairs t2)]
      (table.insert r  v))
    r))

(fn autotile.parse-bitmap [text w]
  (let [l (length text)
        t []
        t2 []
        t3 []]
    (for [i 1 l]
      (match (string.sub text i (+ i 0))
        "\n" :skip
        " " :skip
        tile (table.insert t tile)))
    (for [i 1 (length t)]
      (when (and (= 1 (% (math.floor (/ i (* 3 w))) 3))
                 (= 2 (% (math.floor (% i (* 3 w))) 3)))
        (table.insert t2 (n t i (* 3 w) "_"))))
    (each [i list (ipairs t2)]
      (let [r [0]]
        (fn increment-r [k]
          (each [i v (ipairs r)]
            (tset r i (+ v (^ 2 k)))))
        (fn expand-r [k]
          (local l (length r))
          (for [i 1 l]
            (local v (. r i))
            (tset r i (+ v (^ 2 k)))
            (table.insert r v)))
        (each [k l (ipairs list)]
          (match l
            :0 :nil
            :1 (increment-r (- k 1))
            :_  (expand-r (- k 1))))
        (each [_ v (ipairs r)]
          (tset t3 v i))))
    t3))

(fn autotile.parse [text w]
  (let [l (length text)
        t []]
    (for [i 1 l]
      ;; match is really slow in lua or is it string.sub
      (local tile (string.sub text i (+ i 0)))
      (when (not (or (= tile "\n") (= tile " ")))
        (table.insert t tile)
        )
      ;; (match (string.sub text i (+ i 0))
      ;;   "\n" :skip
      ;;   " " :skip
      ;;   tile (table.insert t tile))
      )
    t))

(fn autotile.neighbours [map width]
  (let [t []
      d "_"
      height (math.floor (/ (# map) width))
      w (- width 0)
      h (- height 1)]
    (for [r 0 h]
      (for [c 1 w]
      ;; fewer-branches? maybe a hashtable approach?
      (table.insert
       t
       [
        ;; c
        (. map (+ 0 (+ c 0) (* (+ r 0) w)))
        ;; tl
        (if (or (= r 0) (= c 1)) d
            (. map (+ 0 (+ c -1) (* (+ r -1) w))))
        ;; t
        (if (= r 0) d
            (. map (+ 0 (+ c 0) (* (+ r -1) w))))
        ;; tr
        (if (or (= r 0) (= c w)) d
            (. map (+ 0 (+ c 1) (* (+ r -1) w))))
        ;; r
        (if (= c w) d
            (. map (+ 0 (+ c 1) (* (+ r 0) w))))
        ;; br
        (if (or (= r h) (= c w)) d
            (. map (+ 0 (+ c 1) (* (+ r 1) w))))
        ;; b
        (if (= r h) d
            (. map (+ 0 (+ c 0) (* (+ r 1) w))))
        ;; bl
        (if (or (= r h) (= c 1)) d
            (. map (+ 0 (+ c -1) (* (+ r 1) w))))
        ;; l
        (if (= c 1) d
            (. map (+ 0 (+ c -1) (* (+ r 0) w))))])))
    t))

;; (fn autotile.neighbours [map w]
;;   (let [t []]
;;     (each [i value (ipairs map)]
;;       (table.insert t (f-n map i w "_")))
;;     t))

(fn autotile.match-key-fast [map width bitmap]
  (let [t []
        height (math.floor (/ (# map) width))
        w (- width 0)
        h (- height 1)]
    (for [r 0 h]
      (for [c 1 w]
        ;; there are a lot of branches in this inner loop, is there
        ;; a way to speed this up?
        (local ch (. map (+ 0 (+ c 0) (* (+ r 0) w))))
        (var j 0)
        ;; tl
        (if (or (= r 0) (= c 1)
                (= ch (. map (+ 0 (+ c -1) (* (+ r -1) w)))))
            (set j (+ j (^ 2 0))))
        ;; t
        (if (or (= r 0)
                (= ch (. map (+ 0 (+ c 0) (* (+ r -1) w)))))
            (set j (+ j (^ 2 1))))
        ;; tr
        (if (or (= r 0) (= c w)
                (= ch (. map (+ 0 (+ c 1) (* (+ r -1) w)))))
            (set j (+ j (^ 2 2)))
            )
        ;; r
        (if (or (= c w)
                (= ch (. map (+ 0 (+ c 1) (* (+ r 0) w)))))
            (set j (+ j (^ 2 3)))
            )
        ;; br
        (if (or (= r h) (= c w)
                (= ch (. map (+ 0 (+ c 1) (* (+ r 1) w)))))
            (set j (+ j (^ 2 4)))
            )
        ;; b
        (if (or (= r h)
                (= ch (. map (+ 0 (+ c 0) (* (+ r 1) w)))))
            (set j (+ j (^ 2 5)))
            )
        ;; bl
        (if (or (= r h) (= c 1)
                (= ch (. map (+ 0 (+ c -1) (* (+ r 1) w)))))
            (set j (+ j (^ 2 6)))
            )
        ;; l
        (if (or (= c 1)
                (= ch (. map (+ 0 (+ c -1) (* (+ r 0) w)))))
            (set j (+ j (^ 2 7)))
            )
        (table.insert t {:x c
                         :y (+ 1 r)
                         :bitmap-index (or (. bitmap ch j) "NA")
                         :char ch
                         :ignore "NA"})
        ))
    t))

(fn autotile.match-key [map w char _?]
  (let [t []]
    (each [_ row (ipairs map)]
      (var j 0)
      (local v (. row 1))
      (each [i value (ipairs row)]
        (when (> i 1)
          (match value
            "_" (when _? (set j (+ j (^ 2 (- i 2)))))
            char (set j (+ j (^ 2 (- i 2))))
            _ :pass
            )))
      (when (~= v char)
          (set j "_"))
      (table.insert t j)
      )
    t))

(fn autotile.match-bitmap [map bitmap name]
  (let [t []]
    (each [_ v (ipairs map)]
      (local o (. bitmap v))
      
      (table.insert t (or o "NA")))
    t))

(fn autotile.to-sparse [map w char ignore]
  (let [s {}]
    (each [i index (ipairs map)]
      (autotile.set-index s i w
                          :bitmap-index index
                          :char char                          
                          :ignore ignore))
    s))

(fn autotile.set-brushes [map brush-map]  
  (each [i sparse (ipairs map)]
    (tset sparse :brush (. brush-map sparse.char)))
  map)

(fn autotile.merge [map ...]
  (each [_ m (ipairs [...])]
    (each [k v (ipairs m)]
      (when (= "NA" (. map k :bitmap-index))
        (tset map k v))))
  map)

(fn autotile.quad-regions [bitmap w px py pw ph iw ih]
  (var quad-regions [])
  (each [_ key (pairs bitmap)]
    (tset quad-regions key {:x (+ px (* pw (math.floor (% (- key 1) w))))
                            :y (+ py (* ph (math.floor (/ (- key 1) w))))
                            :w pw
                            :h ph
                            : iw
                            : ih
                            }))
  quad-regions)

(fn autotile.re-autotile [sparse-map w params]
  (let [t []
        bitmaps {}]
    (each [_ {:char value} (ipairs sparse-map)]
      (table.insert t value))
    (each [_ [char bitmap bitmap-w] (ipairs params)]
      (tset bitmaps char (autotile.parse-bitmap bitmap bitmap-w)))
    (local ret (autotile.match-key-fast t w bitmaps))
    ret
    ))

(fn autotile.decode [str w params]
  ;; -> params [[char bitmap bitmap-w][char bitmap bitmap-w]]  
  (let [bitmaps {}]
    (each [_ [char bitmap bitmap-w] (ipairs params)]
      (tset bitmaps char (autotile.parse-bitmap bitmap bitmap-w)))
    (autotile.match-key-fast (autotile.parse str w) w bitmaps)))


(fn autotile.decode-old [str w params]
  ;; -> params [[char bitmap bitmap-w][char bitmap bitmap-w]]
  (var t false)
  (each [_ [char bitmap bitmap-w] (ipairs  params)]
    (local d
           (-> str
               (autotile.parse w)
               (autotile.neighbours w)
               (autotile.match-key w char true)
               (autotile.match-bitmap (autotile.parse-bitmap bitmap bitmap-w))
               (autotile.to-sparse w char "NA")
               ))
    (if t
        (autotile.merge t d)
        (set t d)
        )
    )
  t)

(fn autotile.encode [sparse-map]
  (var row 1)
  (var ret "")
  (each [key value (ipairs sparse-map)]
    (when (> value.y row)
      (set row value.y)
      (set ret (.. ret "\n"))
      )
    (set ret (.. ret value.char))
    )
  ret)

autotile
