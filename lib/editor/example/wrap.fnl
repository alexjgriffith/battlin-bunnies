(require :editor)

(local image-filename "atlas.png")

(local image (love.graphics.newImage image-filename))

(local json (require :lib.json))

(fn parse-slice [slices]
  (let [ret {}]
    (each [_ s (ipairs slices)]
      (let [{: x : y : w : h} (. s :keys 1 :bounds)]
        (tset ret s.name
              {:name s.name : x : y : w : h})
        ))
    ret))

(fn to-quads [slices]
  (let [ret {}]
    (each [name s (pairs slices)]
      (let [{: x : y : w : h} s]
        (tset ret s.name (love.graphics.newQuad x y w h 256 256))))
    ret))

(local slices
       (-> "atlas.json"
           love.filesystem.read
           json.decode
           (. :meta :slices)
           parse-slice))

(local quads (to-quads slices))

(fn love.draw []
  (love.graphics.draw image quads.IceHouse))
