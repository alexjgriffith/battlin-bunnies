;; Sprite Pipeline
;; Author: AlexJGriffith
;; Description: Easily render sprites with multiple shader passes
;; Licence: MIT

(local lg love.graphics)

(local __canvases {})

(fn __get-canvas [key]
  (assert (. __canvases key) (.. "No canvas for key: " key))
  (. __canvases key))

(fn create-index [w h]
  "Create pipeline canvases and return a unique index for the pair.
If the canvas size is already present it will return the index for
the pair that were already created.
W H Dimensions of the sprite being drawn
returns KEY a string"
  (let [key (.. "[" w " " h "]")]
    (when (not (. __canvases key))
      (tset __canvases key [])
      (table.insert (. __canvases key) (lg.newCanvas w h))
      (table.insert (. __canvases key) (lg.newCanvas w h)))
    key))

(fn release-index [key]
  "Release the canvases for index key."
  (match (__get-canvas key)
    [canvas-a canvas-b] (do (canvas-a:release) (canvas-b:release))))

(fn start [callback index ox oy]
  "Start the sprite rendering pipeline.
CALLBACK  a callback function that utilizes draw commands
INDEX     the index generated from add-canvas
returns [canvas-a canvas-b]"
  (let [[canvas-a canvas-b] (__get-canvas index)]
    (lg.push :all)
    (lg.reset)
    (lg.translate ox oy)
    (lg.setCanvas canvas-b)
    (lg.clear)
    (lg.setCanvas canvas-a)
    (lg.clear)
    (callback)
    (lg.setCanvas)
    (lg.pop)
    ;; creates garbage :/
    [canvas-a canvas-b]))

(fn __pass-args [shader args]
  (when (and shader args)
    (each [key value (pairs args)]
      (when (shader:hasUniform key) (shader:send key value)))))

(fn direct [callback canvas-target shader? args? post-args? x y sx sy]
  (lg.push :all)
  (lg.setCanvas canvas-target)
  (lg.translate (math.floor x) (math.floor y))
  (when (and sx sy) (lg.scale sx sy))
  (lg.setShader shader?)
  (__pass-args shader? args?)
  (callback)
  (__pass-args shader? post-args?)
  (lg.pop)
  canvas-target)

(fn shader-pass [[canvas-from canvas-to ox oy] shader? args? post-args?]
  "A sprite shader pass.
CANVAS-FROM  the canvas that is to be drawn
CANVAS-TO    the target canvas
SHADER?      the shader to use (optional)
ARGS?        arguments to the shader (optional)
POST-ARGS?   arguments to pass tot he shader after drawing (optional)
returns [canvas-to canvas-from]"
  (lg.push :all)
  (lg.reset)
  (lg.setCanvas canvas-to)
  (lg.setShader shader?)
  (when (and shader? args?)
    (each [key value (pairs args?)]
      (when (shader?:hasUniform key) (shader?:send key value))))
  (lg.draw canvas-from)
  (when (and shader? post-args?)
    (each [key value (pairs post-args?)]
      (if (shader?:hasUniform key)
          (shader?:send key value)
          ;; Maybe make this an assert?
          ;;(print (.. "SHADER ERROR! - " key " is not a uniform in shader."))
          )))
  (lg.setCanvas canvas-from)
  (lg.clear)
  (lg.setCanvas)
  (lg.pop)
  [canvas-to canvas-from])

(fn end [[canvas-from _] canvas-to x y ox oy shader?]
  "Draw shaded sprite to final target canvas at postion x y.
CANVAS-TO  target canvas or screen
X Y        top left transform for sprite being drawn
returns canvas-to"
  (lg.push :all)
  ;; (lg.reset)
  (lg.setShader shader?)
  (lg.setCanvas canvas-to)
  (lg.draw canvas-from (- x ox) (- y oy))
  (lg.pop)
  canvas-to)

{: create-index
 : release-index
 : start
 : end
 : shader-pass
 : direct
 : __canvases
 : __get-canvas
 : __pass-args}
