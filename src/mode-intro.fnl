(local lg love.graphics)
(local gamestate (require :lib.gamestate))
(local vink (require :lib.vink24))
(local {: fonts : atlas} (require :assets))
(local {: get-mouse-info : button : click-text} (require :ui))
(local fade (require :lib.fade))
(local {: fade-out-switch : fade-out-push } (require :transition))

{:enter (fn enter [obj]
          (local {: sounds} (require :assets))
          (local state (require :state))
          (set state.volume 0.2)
          (when (~= sounds.bgm sounds.title)
            (when sounds.bgm (sounds.bgm:stop))
            (tset sounds :bgm sounds.title)
            (sounds.bgm:stop)
            (sounds.bgm:play))
          (fade.in))
 
 :draw (fn draw [obj message]
         (love.graphics.setBackgroundColor vink.purple-4)
         (lg.setColor vink.beige-1)
         (lg.setFont fonts.main-title)
         (lg.printf "Bunny Buddies" 0 (- 256 0) 1280 :center)
         (lg.setFont fonts.text)
         ;; (lg.printf "Lisp Game Jam Entry 2023" 0 (- 340 0) 1280 :center)
         (lg.printf "Can you befriend them all?" 0 (- 340 0) 1280 :center)
         (local mouse-info (get-mouse-info))
         (click-text mouse-info "Alexjgriffith" 20 664 1280 :left
                     (fn [] (love.system.openURL "https://alexjgriffith.itch.io")))
         ;;(lg.draw atlas.image atlas.quads.Loaf (- 640 32) 400)
         (lg.draw atlas.image atlas.quads.BigBunny (- 640 (* 5 32) 32 400) (- 400 240))
         (button mouse-info :Play 500 (fade-out-switch :mode-world
                                                       {:text :LOADING :font fonts.main-title :ellipsis "" :timer 0}))
         (button mouse-info :Credits 590 (fade-out-push :mode-credits))
         (lg.draw fade.canvas))
 
 :update (fn update [obj dt set-mode])
 
 :keypressed (fn keypressed [key set-mode]
               (when (not (love.keyboard.isDown "lctrl" "rctrl" "capslock"))
                 (match key _ :nothing)))}
