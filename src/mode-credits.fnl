(local lg love.graphics)
(local vink (require :lib.vink24))
(local {: fonts : atlas} (require :assets))
(local gamestate (require :lib.gamestate))
(local fade (require :lib.fade))
(local {: fade-pop } (require :transition))

(local credits [{:title "Game Code"
                 :author "AlexJGriffith"
                 :modified "N/A"
                 :licence "GPL3+"
                 :link "gitlab.com/alexjgriffith/battlin-bunnies"}
                {:title "Game Art"
                 :author "AlexJGriffith"
                 :modified "N/A"
                 :licence "CCBY-4.0"
                 :link "gitlab.com/alexjgriffith/battlin-bunnies"}
                {:title "Music"
                 :author "SubspaceAudio"
                 :modified "No"
                 :licence "CC0"
                 :link "opengameart.org/content/5-chiptunes-action"}
                {:title "SFX"
                 :author ""
                 :modified "No"
                 :licence ""
                 :link ""}
                {:title "Icons"
                 :author ""
                 :modified "No"
                 :licence ""
                 :link ""}
                {:title "Engine"
                 :author ""
                 :modified "No"
                 :licence ""
                 :link ""}])

{:enter (fn enter [obj] (fade.in))
 :draw (fn draw [obj]
         (local {: get-mouse-info : button : draw-credit} (require :ui))
         (love.graphics.setBackgroundColor vink.purple-4)
         (lg.setColor vink.beige-1)
         (lg.setFont fonts.main-title)
         (lg.printf "Credits" 0 20 1280 :center)
         (local mouse-info (get-mouse-info))
         (button mouse-info :Back 640 (fade-pop))
         (for [i 1 6]
           (when (. credits i)
             (draw-credit mouse-info
                          (+ 10 (if (= 0 (% i 2)) 650 0))
                          (+ 100  (* 180 (-> i (- 1) (/ 2) (math.floor))))
                          (. credits i))))
         (lg.draw fade.canvas))
         
 :update (fn update [obj dt set-mode])
 :keypressed (fn keypressed [gstate key set-mode]
               (when (not (love.keyboard.isDown "lctrl" "rctrl" "capslock"))
                 (match key :escape ((fade-pop)))))
 }
