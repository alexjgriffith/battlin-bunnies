(local lg love.graphics)
(local vink (require :lib.vink24))
(local {: fonts : atlas} (require :assets))
(local gizmo (require :gizmo))
(local {: fade-pop } (require :transition))
(local fade (require :lib.fade))
(local bump (require :lib.bump))


(local world (bump.newWorld 128))
(local voids [{:x 130 :y 130}
              
              {:x 600 :y 200}
              {:x 600 :y 264}
              {:x 664 :y 200}
              {:x 664 :y 264}
              
              {:x 400 :y 400}
              {:x 400 :y 464}
              {:x 400 :y 528}
              {:x 400 :y (+ 64 528)}
              {:x 400 :y (+ (* 2 64) 528)}])

(each [_ {: x : y &as void} (ipairs voids)]
  (world:add void (- x 0) (- y 0) 64 64))

(fn circle [min max tableleft?]
  (fn [ux uy distance]
    (if (< distance min)
        ;;(values ux uy)
        (values (- ux) (- uy))
        (> distance max)
        (values ux uy)
        (if (and tableleft? tableleft?.left)
            (values (+ uy) (- ux))
            (values (- uy) (+ ux)))
        )))

(fn chase []
  (fn [ux uy distance]
    (values ux uy)))

(fn flee []
  (fn [ux uy distance]
    (values (- ux) (- uy))))

(local (w h _flags) (love.window.getMode))
(local bunny {:w 64
              :h 64
              :max-distance (* 1.5 128)
              :sense-distance (* 10 128)
              :cx -32
              :name :Loaf
              :quad (. atlas.quads :Loaf)
              :type :bunny
              :cy -32
              :cw 64
              :ch 64
              :ox -32
              :oy -32
              :speed 5
              :weights []
              :void-weights []
              :t-voids []
              :f-voids []
              :direction-index 1
              :x (/ w 2) :y (/ h 2)
              :left true :current-mode :chase
              :mode-order [:chase :cirlce :flee]})

(tset bunny :modes {:chase (chase) :cirlce (circle 200 300 bunny) :flee (flee)})


(world:add bunny (+ bunny.x bunny.cx) (+ bunny.y bunny.cy)
           bunny.cw bunny.ch)


(fn euclidean-distance [x1 y1 x2 y2]
  (math.sqrt (+ (^ (- x1 x2) 2) (^ (- y1 y2) 2))))

(fn index-to-vector  [index velocity distance-to-target angle-count speed]
  (let [angle (* (- index 1) (/ (* 2 math.pi) angle-count))
        radius (if (< distance-to-target (* speed 4)) (/ distance-to-target  (* speed 4)) 1)
        x (* (math.cos angle) radius)
        y (* (math.sin angle) radius)]
    (values x y)))

(fn next-mode [self]
  (var mode-index 1)
  (each [key value (ipairs self.mode-order)]
    (when (= value self.current-mode)
      (set mode-index key)))
  (set self.current-mode (. self.mode-order (+ (% (- (+ mode-index 1) 1) (# self.mode-order)) 1))))

(fn draw-colliders []
  (local items (world:getItems))
  (each [_ item (ipairs items)]
    (local (x y w h) (world:getRect item))
    (lg.push :all)
    (lg.setColor 1 1 0 1)
    (lg.rectangle :line x y w h)
    (lg.pop)))

(fn draw [self]
  (lg.push)
  (lg.setColor 1 1 1 1)
  (lg.draw atlas.image self.quad (+ bunny.x self.oy) (+ self.y self.ox))
  (lg.pop))

(fn update [self dt]
  (local directional (require :directional))
  (local (mx my) (love.mouse.getPosition))
  (local max-distance self.max-distance) ;; (* 1.5 128)
  (local sense-distance self.sense-distance) ;;(* 10 128)
  (local voids (world:queryRect (- self.x max-distance) (- self.y max-distance) (* max-distance 2) (* max-distance 2) (fn [item] (~= item.name self.name))))
  (local t-voids [])
  (each [i {: x : y &as v} (ipairs voids)]
    (tset t-voids i {:x (+ x 32) :y (+ y 32)}))
  (set self.t-voids t-voids)
  (local f-voids [])
  (each [i {: x : y &as v} (ipairs t-voids)]
    (when (> max-distance (euclidean-distance x y self.x self.y))
      (table.insert f-voids v)))
  (set self.f-voids f-voids)
  (local within-sense-distance (> sense-distance (euclidean-distance mx my self.x self.y)))
  (local (move index weights void-weights)
         (directional.update
          self.x self.y
          {:x (if within-sense-distance mx self.x)
           :y (if within-sense-distance my self.y)}
          f-voids
          (. self.modes self.current-mode)
          (fn [ux uy _distance]
            (values ux uy))
          max-distance))
  (set self.weights weights)
  (set self.void-weights void-weights)
  (set self.direction-index index)
  ;; bounce
  (when (< (. weights index) (+ 0.6 (* 0.3 (math.random))))
    (set self.left (not self.left)))
  ;; move
  (let [speed (* dt 60 bunny.speed)
        (x y) (index-to-vector index (. weights index)
                               (euclidean-distance mx my self.x self.y)
                               (# weights)
                               speed)]
    (when move
      (let [tx (+ (+ self.x self.cx) (* speed x))
            ty (+ (+ self.y self.cy) (* speed y))
            (ax ay cols len) (world:move self tx ty)]
        (tset self :x (- ax self.cx ))
        (tset self :y (- ay self.cy ))
        (when (> len 0) (set self.left (not self.left)))
        ))
    )
  )

(fn debug-draw [self]
  (local max-distance self.max-distance)
  (local t-voids self.t-voids)
  (local f-voids self.f-voids)
  (lg.setColor 1 1 1 1)
  (local {: fonts } (require :assets))
  (lg.setFont fonts.text)
  (gizmo.draw-direction-gizmo
   self
   (icollect [_ w (ipairs self.weights)] (* w 100))
   (icollect [_ w (ipairs self.void-weights)] (* w 100))
   self.direction-index)
  (lg.circle :line self.x self.y self.max-distance)
  (each [key {: x : y} (ipairs t-voids)]
    (lg.circle :fill x y 10))
  (lg.setColor 1 0 0 1)
  (each [key {: x : y} (ipairs f-voids)]
    (lg.circle :fill x y 10)))

{:enter (fn enter [obj] (fade.in))
 :draw (fn [obj message]
         (love.graphics.setBackgroundColor vink.purple-4)
         (lg.push :all)
         (lg.setColor 1 1 1 1)
         (draw bunny)
         ;; (debug-draw bunny)
         (draw-colliders)
         (lg.pop)
         (lg.draw fade.canvas)
         )
 :update (fn [obj dt ] (update bunny dt))
 :keypressed (fn keypressed [gstate key set-mode]
               (local gamestate (require :lib.gamestate))
               (when (not (love.keyboard.isDown "lctrl" "rctrl" "capslock"))
                 (match key
                   :tab (next-mode bunny)
                   :escape ((fade-pop)))))}
