(local {: preview : draw : add-sprite-pipeline-canvases
        : draw-water : add-collider}
       (require :prefab-object))

(fn draw-callback [self image ...]
  (fn []
    (local lg love.graphics)
    (lg.push :all)
    (lg.setColor 1 1 1 1)
    (lg.draw image self.quad)
    (lg.pop)))

(fn update [self dt]
  (local {: player} (require :state))
  (when (> (# player.bunnies) 0)
    (set self.quad self.down-quad)
    (set self.cross true)))

(local wall {:serialize (fn [self] self.encode)
             :handlers {}
             : preview
             : draw-water
             : update
             : draw
             : draw-callback})

(local wall-mt {:__index wall})

(fn wall.create [brush]
  (pp :creating-wall)
  (local {: x : y : name} brush)
  (local {: atlas} (require :assets))
  (local assets (require :assets))
   (-> {:encode brush
        :name name
        :type :wall
        :active true
        :down-quad (. atlas.quads :BlockDown)
        :up-quad (. atlas.quads :BlockUp)
        :quad (. atlas.quads :BlockUp)
        :cross false
        :x x :y y
        :w 64 :h 192}
       ((fn [x] (setmetatable x wall-mt)))
       add-sprite-pipeline-canvases
       add-collider
       ))

wall
