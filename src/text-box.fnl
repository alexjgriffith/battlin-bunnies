(fn scollect [st key]
  (let [accu []
        iterator (string.gmatch st key)]
    (var v nil)
    (while (do (set v [(iterator)]) (> (# v) 0))
      (table.insert accu (. v 1))
      )
    accu))

(local parse-string "[%w.,!\\//?']+")

(local test-message "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lacinia gravida purus, nec aliquam enim malesuada nec.,~\\/")

(local lg love.graphics)

(local {: fonts} (require :assets))

(local words (scollect test-message parse-string))
(local f fonts.small-text)
(local t (lg.newText f " "))
(local space-h (t:getHeight))
(local space-w (t:getWidth))

(fn test-wrap [width c str?]
  (let [str (or str? test-message)
        iter (string.gmatch str parse-string)]
    (var x 0)
    (var y 0)
    (var c-count 0)
    (var ret true)
    (each [word iter]
      (t:set word)
      (local ww (t:getWidth))
      (local wl (# word))
      (when (>  (+ x ww) width)
          (set x 0)
          (set y (+ y space-h)))
      (if (< (+ c-count wl) c) (lg.draw t x y)
          (< c-count c) (do (t:set (string.sub word 1 (- c c-count))) (lg.draw t x y))
          (>= c-count c) (set ret false))
      (set c-count (+ c-count 1 wl))
      (set x (+ x space-w ww)))
    ret))

{: test-wrap}
