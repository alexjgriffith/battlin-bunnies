(local lg love.graphics)
(local gamestate (require :lib.gamestate))
(local vink (require :lib.vink24))
(local {: fonts : atlas} (require :assets))
(local {: get-mouse-info : button : click-text} (require :ui))
(local fade (require :lib.fade))
(local {: fade-out-switch : fade-out-push } (require :transition))

(local {: editor : brush} (require :editor-setup))

(local shaders (require :shaders))
(tset shaders :grass-shadow-shader (love.graphics.newShader :assets/shaders/grass-shadow.glsl))
(tset shaders :grass-shader (love.graphics.newShader :assets/shaders/grass.glsl))
(tset shaders :reflection-shader (love.graphics.newShader :assets/shaders/reflection.glsl))

(fn get-obj [level layer name]
  (var ret nil)
  (let [data (. level :layers layer :data)]
    (each [index obj (ipairs data)]
      (when (= obj.name name) (set ret obj))))
  ret)

(fn reset-level [level-name]
  (local bump (require :lib.bump))  
  (local state (require :state))
  (local level (require :level))
  (tset state :world (bump.newWorld))
  (tset state :camera {:x 0 :y 0 :scale 1 :angle 0})
  (tset state :target {:x 0 :y 0})
  (tset state :editing false)
  ;;(tset state :level (level.empty-level))
  (tset state :level (level.load :l1))
  (tset state :player (get-obj state.level :objects :Player))
  )

(set _G.rl reset-level)

(local masks (require :masks))

(fn generate-masks []
  (let [state (require :state)]
    (tset state :grass-canvas (masks.make-mask :grass))))

;; (fn draw-mask [mask-name]
;;   (let [state (require :state)
;;         mask (. state mask-name)]
;;     (lg.push)
;;     (lg.setColor 1 1 1 0.5)
;;     (lg.draw mask)
;;     ;; (lg.setColor 1 1 1 1)
;;     (lg.pop)))


(fn enter [obj]
  (local {: sounds} (require :assets))
  (local state (require :state))
  (set state.volume 0.2)
  (reset-level :main-level)
  (generate-masks)
  
  (when (~= sounds.bgm sounds.l2)
    (when sounds.bgm (sounds.bgm:stop))
    (tset sounds :bgm sounds.l2)
    (sounds.bgm:stop)
    (sounds.bgm:play))  
  (editor.init state.level {:x 0 :y 0 :scale 1 :angle 0})
  (fade.in))

(var white-timer 0)

(fn draw [obj]
  (local state (require :state))
  (fn draw-colliders []
    (local items (state.world:getItems))
    (local camera state.camera) 
    (let [s camera.scale]
      (each [_ item (ipairs items)]
        (local (x y w h) (state.world:getRect item))
        (lg.push :all)
        (lg.setColor 1 1 0 0.5)
        (lg.translate (- camera.x) (- camera.y))
        (lg.rectangle :line (* x s) (* y s) (* w s) (* h s))
        (lg.pop))))
  
  ;; 95, 205, 228
  (lg.setBackgroundColor (/ 98 256) (/ 168 256) (/ 248 256) 1)
  (lg.setBackgroundColor (/ 95 256) (/ 205 256) (/ 228 256) 1)
  ;; draw
  (lg.push :all)
  (local grass-canvas (masks.draw-mask :grass state.camera.x state.camera.y 1280 720))
  (lg.translate (- state.camera.x) (- state.camera.y))
  (lg.setColor 1 1 1 1)
  (state.level:draw-layer :ground)
  (state.level:draw-layer-object-method :objects :draw-home)
  (state.level:draw-layer :buttons)
  
  (do ;; draw objects with grass shader
    (lg.setShader shaders.grass-shader)
    (local {:__pass-args pass-args} (require :lib.sprite-pipeline))
    (local assets (require :assets))
    (local ({:iw iw :ih ih}) assets.atlas.brushes.Player)
    (lg.setShader shaders.grass-shadow-shader)
    (pass-args shaders.grass-shadow-shader
               {:camera [state.camera.x state.camera.y]
                :mask state.grass-canvas
                :maskSize [1280 720]})
    (state.level:draw-layer-object-method :objects :draw-shadow)
    (pass-args shaders.grass-shader
               {:camera [state.camera.x state.camera.y]
                :mask state.grass-canvas
                :maskSize [1280 720]
                :atlasSize [iw ih]
                :scale 1})
    (state.level:draw-layer :objects))
  (lg.setShader)
  (state.player:draw-arch)
  (state.level:draw-layer-object-method :objects :draw-emoji)
  (state.level:draw-layer-object-method :objects :draw-text)
  (when state.editing (state.level:draw-layer-object-method :objects :debug-draw))
  (state.level:draw-layer-object-method :objects :draw-interaction-prompt)
  (lg.setLineWidth 4)
  (lg.pop)
  ;;(lg.setColor 1 1 1 0.2)
  ;; (lg.draw grass-canvas)
  (lg.setColor 1 1 1 1)
  (do ;; throwables
    (lg.push :all)
    (lg.setFont fonts.small-text)
    (if (> white-timer 0)
        (lg.setColor 0 0 0 1)
        (lg.setColor 0.1 0.1 0.1 0.1))
    
    (lg.rectangle :fill 15 15 125 55 10)
    (if (> white-timer 0)
        (lg.setColor 1 1 1 1)
        (lg.setColor 1 1 1 1))
    
    (lg.print "Tab" 25 25)
    (lg.draw atlas.image atlas.quads.SmallBall 75 25)
    (lg.draw atlas.image atlas.quads.SmallBone 100 25)
    (if (> white-timer 0)
        (lg.setColor 1 1 1 1)
        (lg.setColor vink.purple-4))
    
    (lg.setLineWidth 3)
    (match state.player.throw.what
      :ball (lg.line 87 60 97 60)
      :food (lg.line (+ 25 87) 60 (+ 25 97) 60))
    (lg.pop))

  (do ;; friends
    (local w 160)
    (local x (- 1280 w 15))
    (lg.push :all)
    (lg.setFont fonts.small-text)
    (lg.setColor 0.1 0.1 0.1 0.1)
    (lg.rectangle :fill x 15 w 55 10)
    (lg.setColor 1 1 1 1)
    (lg.print "Bunnies" (+ x 15) 25)
    (lg.print (# state.player.bunnies) (+ x 15 100) 25)
    (lg.print "5" (+ x 15 120) 35)
    (lg.setLineWidth 3)
    (lg.line (+ x 25 100) 60 (+ x 40 100) 30)
    
    (lg.pop)
    )
  
  (when state.editing (editor.draw) (draw-colliders))
  (lg.setColor 0 0 0 1)
  (lg.setFont fonts.small-text)
  (when dev (lg.print (.. "FPS:" (love.timer.getFPS)) 10 (- 720 30)))
  (lg.setColor 1 1 1 1)
  (lg.draw fade.canvas))

(fn update [obj dt]
  (set white-timer (- white-timer dt))
  (local state (require :state))  
  (state.level:update-layer :objects dt)
  (state.level:y-sort-layer :objects)
  (state.level:update-layer :buttons)
  (local camera state.camera)
  (editor.update dt {:x (- camera.x) :y (- camera.y) :scale 1})
  ;; can make a better camera later
  (local (w h) (love.window.getMode))
  (set state.camera.x (math.floor (- state.player.x -32 (/ w 2))))
  (set state.camera.y (math.floor (- state.player.y -64 (/ h 2))))
  
  (set state.balls (icollect [key b (ipairs state.balls)] (when (> b.time 0) b)))
  ;;(set state.balls [])
  )

(fn mousepressed [self x y button]
  (local state (require :state))
  (when state.editing (editor.mousepressed x y button)))

(fn mousereleased [self x y button]
  (local state (require :state))
  (when state.editing (editor.mousereleased x y button)))

{: enter
 : draw
 : update
 : mousepressed
 : mousereleased
 :keypressed (fn keypressed [gstate key code]
               (local state (require :state))
               (local interactions (require :interactions))
               (local captured? (interactions.call key state.player))
               (interactions.clear "i")
               (interactions.clear "escape")
               (when (and (not captured?) (not (love.keyboard.isDown "lctrl" "rctrl" "capslock")))
                 (match key
                   ;; :escape ((fade-out-switch :mode-intro))
                   :tab (do
                          (local {: sounds} (require :assets))
                          (sounds.bell_ding3:stop)
                          (sounds.bell_ding3:play)
                          (set white-timer 0.2)
                          (state.player:switch-throw))
                   :l (do
                          (when state.editing
                        (pp (.. "Saving as: " (.. state.level.name ".fnl")))
                        (state.level:save (.. (love.filesystem.getSource) "src/" state.level.name ".fnl"))))
                   _ (editor.keypressed key code))))
 }
