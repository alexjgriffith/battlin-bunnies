(fn grass-shader-pipeline [self callback dx? dy?]
  (let [lg love.graphics
        sp (require :lib.sprite-pipeline)
        (px py pw ph) (self.quad:getViewport)
        {: grass-shader} (require :shaders)
        canvas (lg.getCanvas)
        dx (or dx? 0)
        dy (or dy? 0)
        ox self.canvas-offset.x
        oy self.canvas-offset.y]
    (-> callback (sp.direct canvas grass-shader
                            {:spriteAtlasPosition [px py]
                             :spriteSize [pw ph]
                             :depth (or self.depth 4)
                             :above 0.0}
                            nil (- self.x dx) (- self.y dy)))))

(fn water-flip-pipeline [self callback dx? dy?]
  (let [lg love.graphics
        sp (require :lib.sprite-pipeline)
        (px py pw ph) (self.quad:getViewport)
        {: reflection-shader} (require :shaders)
        canvas (lg.getCanvas)
        dx (or dx? 0)
        dy (or dy? 0)
        ox self.canvas-offset.x
        oy self.canvas-offset.y]
    (-> callback (sp.direct canvas reflection-shader nil nil (- self.x dx) (- self.y dy (* 2 (- self.h))) 1 -1))))

(local object
       {:serialize (fn [self] self.encode)
        : grass-shader-pipeline
        : water-flip-pipeline
        :draw-callback (fn [self image] (fn [] (love.graphics.draw image self.quad)))
        :draw (fn [self image ...]
                (local {: grass-shader-pipeline} (require :prefab-object))
                (grass-shader-pipeline self (self.draw-callback self image)))
        :draw-water (fn [self image ...]
                      (water-flip-pipeline self (self.draw-callback self image)))
        
        :draw-shadow
        (fn [self image ...]
          (local lg love.graphics)
          (when (and self.shadow self.shadow-quad)
            (love.graphics.setColor 1 1 1 0.6)
            (local grass-shadow (lg.getShader))
            (when (and grass-shadow (grass-shadow:hasUniform :onField))
              (grass-shadow:send :onField false))
            (love.graphics.draw image self.shadow-quad 
                                (+ (or self.sx 0) self.x) (+ self.y (or self.sy 0)))
          (when (and grass-shadow (grass-shadow:hasUniform :onField))
            (grass-shadow:send :onField true)
            (love.graphics.draw image self.shadow-quad
                                (+ (or self.sx 0) self.x)
                                (+ self.y -10 (or self.sy 0)))))
          ;; (when (and self.shadow self.shadow-quad)
                 ;;   (local grass-shadow (lg.getShader))
                 ;;   (if (and self.highlight (= self.highlight 1))
                 ;;       (do (set self.highlight 0)
                 ;;           (love.graphics.setColor 1 1 0 0.2))
                 ;;       (love.graphics.setColor 0 0 0 0.2))
                 ;;   (when (and grass-shadow (grass-shadow:hasUniform :onField)) (grass-shadow:send :onField false))
                   
                 ;;   (love.graphics.draw image self.shadow-quad (+ (or self.sx 0) self.x) (+ self.y (or self.sy 0)))
                 ;;   (when (and grass-shadow (grass-shadow:hasUniform :onField))
                 ;;     (grass-shadow:send :onField true)
                 ;;     (love.graphics.draw image self.shadow-quad (+ (or self.sx 0) self.x) (+ self.y (+ -2 (or self.sy 0))))))
                 )
        
        :draw-outline
        (fn [self image ...]
          (when (and self.outline self.outline-quad)
            (local lg love.graphics)
                   (lg.setColor 1 1 1 0.05)
                   (local quad self.outline-quad)
                   (local grass-shader (lg.getShader))
                   (local (px py pw ph) (quad:getViewport))
                   (when (grass-shader:hasUniform :spriteSize)
                     (grass-shader:send :spriteSize [pw ph]))
                   (when (grass-shader:hasUniform :spriteAtlasPostion)
                     (grass-shader:send :spriteAtlasPosition [px py]))
                   (lg.draw image quad self.x self.y)))
        
        :set-highlight (fn [self] (set self.highlight 1))
        
        :update (fn [self dt])
        
        :handlers {}})

(local object-mt {:__index object})

(fn object.remove [obj]
  (local {: level} (require :state))
  (level:remove-obj :objects obj)
  (let [{: world} (require :state)]
    (when (world:hasItem obj)
      (world:remove obj))))

(fn object.re-add [obj]
  (local {: level} (require :state))
  (level:add :objects obj.x obj.y obj.name))


(fn object.remove-object [obj]
  (let [{: world} (require :state)]
    (when (world:hasItem obj)
        (world:remove obj))))

(fn object.add-sprite-pipeline-canvases [self]
  (let [sprite-pipeline (require :lib.sprite-pipeline)]
    (assert self.w (.. "Object " self.name "is missing w."))
    (assert self.h (.. "Object " self.name "is missing h."))
    (when (not self.canvas-offset)
      (tset self :canvas-offset {:x 16 :y 16 :w 32 :h 32
                                 :oh (or self.encode.oh (+ self.encode.h 32))
                                 :oy (or self.encode.oy 0)
                                 :ow (or self.encode.ow (+ self.encode.w 32))
                                 :ow (or self.encode.ox 0)
                                 }))
    (tset self :sprite-pipeline-index (sprite-pipeline.create-index
                                       (+ self.w self.canvas-offset.w)
                                       (+ self.h self.canvas-offset.h))))
  self)

(fn object.add-collider [obj ?type]
  (let [{: world} (require :state)        
        {: cx : cy : cw : ch} obj.encode]
    (when (and cx cy cw ch)
      (tset obj :collider (or ?type :slide))
      (world:add obj (+ obj.x cx) (+ obj.y cy) cw ch))
    obj))

(fn object.draw-collider [obj ?scale]
  (let [{: world} (require :state)
        lg love.graphics
        scale (or ?scale 1)
        (cx cy cw ch) (if (world:hasItem obj) (world:getRect obj) (values nil nil nil nil))]
    (when (and cx cy cw ch)
      (lg.push :all)
      (lg.setColor 1 1 0 0.5)
      (lg.rectangle :line (* scale cx) (* scale cy) (* scale cw) (* scale ch))
      (lg.pop)
    obj)
  ))

(fn object.preview [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local obj (-> {:quad (love.graphics.newQuad px py pw ph iw ih)
                  : name
                  :encode brush
                  :w brush.w :h brush.h
                  :x brush.x :y brush.y}
                 ((fn [x] (setmetatable x object-mt) x))
                 object.add-sprite-pipeline-canvases
      ))
  obj)


(fn object.create [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local {: atlas} (require :assets))
  (local obj (-> {:encode brush
                  :shadow brush.shadow
                  :shadow-quad (?. atlas.quads brush.shadow)
                  :sy brush.sy
                  :sx brush.sx
                  :outline brush.shadow
                  :outline-quad (?. atlas.quads brush.outline)
                  :name name
                  :highlight 0
                  :quad (love.graphics.newQuad px py pw ph iw ih)
                  :x brush.x :y brush.y
                  :w brush.w :h brush.h}
      ((fn [x] (setmetatable x object-mt) x))
      object.add-collider
      object.add-sprite-pipeline-canvases
      ))
  obj
  )

object
