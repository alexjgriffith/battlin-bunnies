(local fade (require :lib.fade))
(local gamestate (require :lib.gamestate))
(local {: sounds} (require :assets))

(fn fade-out-switch [mode arg]
  (fn []
    (sounds.page:stop)
    (sounds.page:play)
    (fade.out (fn [] (gamestate.switch (require mode))) arg)))

(fn fade-out-push [mode]
  (fn []
    (sounds.page:stop)
    (sounds.page:play)
    (fade.out (fn [] (gamestate.push (require mode))))))

(fn fade-pop []
  (fn []
    (sounds.page:stop)
    (sounds.page:play)
    (fade.out (fn [] (gamestate.pop) (fade.in)))))

{: fade-out-switch : fade-pop : fade-out-push}
