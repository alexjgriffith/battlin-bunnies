(local text {})

;; Tim Sara Mike Alan Ben Kat Will Zach Becca Nora

(local Alan
       {:nobunnies ["Hey Traveller! I'm Alan. Welcome to Bunny Island! If you're down with rabbits you've come to the right place!"
        "On the island we compete to see who can befriend the most wildlife. Ben holds the record at 4! You look like the kind of traveller who could befriend more!"
        "To befriend a rabbit you can throw food and balls. Throwing food will make the bunny no longer flee and playing fetch will help you build friendship."
        "To throw the ball drag the mouse button to build up force. Releasing the mouse will launch the ball like an elastic."
        "Press tab to switch between food and the ball. Here, have as many of each as you'd like :)"
        "Where is the rabbit? They will run and hide in their rabbit holes unless they are familiar with you."
        "To get close try crouching in the tall grass where the rabbit won't see you. Press shift to crouch."
                    "Once you've befriended this lil guy I'll open the gate and you can befriend as many as you can find!"]
        :bunnies ["Looks like you befriended Loaf! Good luck befriending the other four rabbits on the island."]
       :allbunnies ["That's way more bunnies than Ben has befrended! Can't wait to see his face when he sees this!"]})       


(local Mike {:nobunnies [""]
             :bunnies ["Hey Traveller! Looks like you've already made one good friend!"
             "As a fellow rabbit enthusiast I'll let you in on a little secret. It takes 5 pieces of food to make a bunny comfortable with you."
                       "Good luck befriending them all"]
             :allbunnies ["That is a lot of bunnies you've got there Traveller?"]})

(local Sara {:nobunnies [""]
             :bunnies ["Are you looking to befriend more bunnies? None this way!"
                       "Good luck befriending more than Ben!"]
             :allbunnies ["That is a lot of bunnies you've got there Traveller? I think that's more than Ben has!"
                          "Why are these all these rabbits eating bones?"]})

(local Ben {:nobunnies [""]
            :bunnies ["I'm Ben, the best Rabbit befriender on the island! No one befriends bunnies like me!"]
            :allbunnies ["I'm Ben, the best Rabbit befriender on the island! No one befriends bunnies like me!"
                         "Wait, how many bunnies are there with you? 5!?"
                         "I've been bested! You are the bunny befriending champ"]
            })
{: Alan : Mike : Sara : Ben}
