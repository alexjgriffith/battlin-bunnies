(local callbacks {})
(var last-hover nil)
(var last-click nil)
(var hover-count 0)

(fn love.handlers.over [key]
  (local {: sounds} (require :assets))
  (set hover-count (+ hover-count 1))
  (when (~= key last-hover)
    (sounds.bell_ding1:stop)
    (sounds.bell_ding1:play)
    (set last-hover key)))

(fn love.handlers.click [key arg]
  (local {: sounds} (require :assets))
  (when (not last-click)
    (sounds.bell_ding2:stop)
    (sounds.bell_ding2:play)
    (set last-click key)
    ((. callbacks key) arg)))

(fn clear-hover []
  (when (= 0 hover-count) (set last-hover nil))
  (set hover-count 0))
(fn clear-click [] (set last-click nil))


(fn remove-object-wrapper [_ _ obj]
  (local state (require :state))
  (local world state.world)
  (pp (world:hasItem obj))
  :done)

(fn replace-tiles [layer-name data]
  (local {: level : world} (require :state))
  (local items (world:getItems))
  (var cleared 0)
  (each [_ i (ipairs items)]
    (when i.bitmap-index
      (world:remove i)
      (set cleared (+ cleared 1))))
  (local tile-size 64)
  (var cliff 0)
  (var water 0)
  (each [_ d (ipairs data)]
    (match d.char
      :4 (do ;; cliff
               (world:add d (* tile-size (- d.x 1)) (* tile-size (- d.y 1)) tile-size tile-size)
               (set cliff (+ cliff 1)))
      :3 (do ;; water
               (world:add d (* tile-size (- d.x 1)) (* tile-size (- d.y 1)) tile-size tile-size)
               (set water (+ water 1))))))

(fn replace-tile [layer-name]
  (local {: level : world} (require :state))
  (local ground (level:get-layer :ground))
  (local data ground.data)
  (replace-tiles layer-name data))
    
(fn love.handlers.edit-level [key ...]
  (match key
    :remove-object (remove-object-wrapper ...)
    :replace-tile (replace-tile ...)
    :replace-tiles (replace-tiles ...)
    )
  )

(fn love.handlers.ball-land [x y what]
  (fn on-tile-type [self]
  (local {: level} (require :state))
  (let [x (-> self (. :x) (+ 16) (/ 64) (math.floor) (+ 1))
        y (-> self (. :y) (+ 16) (/ 64) (math.floor) (+ 1))]
    
    (if (or (< x 1) (< y 1)) :water
        (. level.layers.ground.data (+ (* 100 (- y 1)) x) :brush))
    ))
  (local {: sounds} (require :assets))
  (match (on-tile-type {: x : y})
    :water (do
             (sounds.splash:stop)
             (sounds.splash:play))
    _ (do
             (sounds.bounce:stop)
             (sounds.bounce:play)))
  (local state (require :state))
  
  (local (_ _ obj) (state.level:add :objects x y (match what :ball
                                                        :SmallBall
                                                        :food :SmallBone)))
  (local state (require :state))
  (table.insert state.balls obj)
  )

{: clear-click : clear-hover : callbacks}
