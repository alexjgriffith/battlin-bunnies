(local {: editor : brush} (require :lib.editor.interface))
(local level (require :lib.editor.level))

(tset brush :brushes  [[:ground :ground :tile]
                       [:ground :grass :tile]
                       [:ground :dirt :tile]
                       [:ground :water :tile]
                       [:ground :cliff :tile]
                       [:objects :Player :object]
                       ])

(tset brush :index 2)

(local ground-brushes
       {:ground {:name :ground
                :bitmap :bitmap-1
                :bitmap-w 1
                :char :0
                :ix 10
                :iy 8}
        :grass {:name :grass
                :bitmap :bitmap-2x2
                :bitmap-w 4
                :char :1
                :ix 0
                :iy 0}
        :dirt   {:name :dirt
                :bitmap :bitmap-2x2
                :bitmap-w 4
                :char :2
                :ix 0
                :iy 5}
        :water {:name :water
                :bitmap :bitmap-2x2
                :bitmap-w 4
                :char :3
                :ix 5
                :iy 5}
        :cliff {:name :cliff
                :bitmap :bitmap-2x2
                :bitmap-w 4
                :char :4
                :ix 5
                :iy 0}
        })

(local player (require :prefab-player))

(local creation-callbacks
       {:Player player.create}
       )

(local preview-callbacks
       {:Player player.preview})

(local walls [:Wall])
(local wall (require :prefab-wall))
(each [_ o (ipairs walls)]
  (tset creation-callbacks o wall.create)
  (tset preview-callbacks o wall.preview)
  (table.insert brush.brushes [:buttons o :object]))


(local objects [:SmallTree :LargeTree :Stump :Rock1 :Rock2 :FenceLeft :FenceCenter :FenceRight :BigTree :Hole :UFence :UFenceDown :UFenceUp])
(local object (require :prefab-object))
(each [_ o (ipairs objects)]
  (tset creation-callbacks o object.create)
  (tset preview-callbacks o object.preview)
  (table.insert brush.brushes [:objects o :object]))

(local characters [:Tim :Sara :Mike :Alan :Ben :Kat :Will :Zach :Becca :Nora])
(local npc (require :prefab-npc))
(each [_ o (ipairs characters)]
  (tset creation-callbacks o npc.create)
  (tset preview-callbacks o npc.preview)
  (table.insert brush.brushes [:objects o :object]))


(local bunnies [:Chonkers :Leggy :Square :Ploof :Loaf :Upright :LilEar :BigEar :Drowsy :Fury])
(local bunny (require :prefab-bunny))
(each [_ o (ipairs bunnies)]
  (tset creation-callbacks o bunny.create)
  (tset preview-callbacks o bunny.preview)
  (table.insert brush.brushes [:objects o :object]))

(local balls [:SmallBall :SmallBone])
(local ball (require :prefab-ball))
(each [_ o (ipairs balls)]
  (tset creation-callbacks o ball.create)
  (tset preview-callbacks o ball.preview)
  (table.insert brush.brushes [:objects o :object])
  )


(tset brush :count (# brush.brushes))

{: editor : brush : level : creation-callbacks : preview-callbacks : ground-brushes}
