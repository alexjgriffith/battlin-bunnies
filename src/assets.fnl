(local lg love.graphics)

(local fonts {})

(local atlas {})

(local masks {})

(local sounds {})

(local icons {})

(local source-type (if web
                       "static"
                       "stream"))
;; BG Music
(tset sounds :title (love.audio.newSource "assets/music/title.ogg" source-type))
(tset sounds :l2 (love.audio.newSource "assets/music/home.ogg" source-type))
(tset sounds :ending (love.audio.newSource "assets/music/ending.ogg" source-type))
(each [_ sound (pairs sounds)]
  (sound:setLooping true)
  (sound:setVolume 0))

(set sounds.bgm sounds.title)

;; SFX
(tset sounds :crunch (love.audio.newSource "assets/sounds/crunch.ogg" "static"))
(tset sounds :splash (love.audio.newSource "assets/sounds/splash.ogg" "static"))
(tset sounds :click (love.audio.newSource "assets/sounds/click.ogg" "static"))
(tset sounds :bounce (love.audio.newSource "assets/sounds/bounce.ogg" "static"))
(tset sounds :hover (love.audio.newSource "assets/sounds/hover.ogg" "static"))
(tset sounds :page (love.audio.newSource "assets/sounds/page.ogg" "static"))
(tset sounds :meaw (love.audio.newSource "assets/sounds/meaw.ogg" "static"))
(tset sounds :land (love.audio.newSource "assets/sounds/land.ogg" "static"))
(tset sounds :cash (love.audio.newSource "assets/sounds/cash.ogg" "static"))
(tset sounds :talk (love.audio.newSource "assets/sounds/talk.ogg" "static"))
(tset sounds :bell_ding1 (love.audio.newSource "assets/sounds/bell_ding1.ogg" "static"))
(tset sounds :bell_ding2 (love.audio.newSource "assets/sounds/bell_ding2.ogg" "static"))
(tset sounds :bell_ding3 (love.audio.newSource "assets/sounds/bell_ding3.ogg" "static"))
(tset sounds :bell_ding4 (love.audio.newSource "assets/sounds/bell_ding4.ogg" "static"))
(sounds.crunch:setVolume 0.2)
(sounds.land:setVolume 0.2)
(sounds.hover:setVolume 0.02)
(sounds.bounce:setVolume 1) ;; why so quiet?
(sounds.splash:setVolume 1) ;; why so quiet?
(sounds.bell_ding1:setVolume 0.2)
(sounds.bell_ding2:setVolume 0.2)
(sounds.bell_ding3:setVolume 0.2)
(sounds.page:setVolume 0.3)
(sounds.talk:setVolume 0.2) 

(set fonts.main-title (lg.newFont :assets/fonts/Virgil.ttf 64))
(set fonts.sub-title (lg.newFont :assets/fonts/Virgil.ttf 48))
(set fonts.text (lg.newFont :assets/fonts/Virgil.ttf 30))
(set fonts.small-text (lg.newFont :assets/fonts/Virgil.ttf 24))

(tset icons :volume-on (lg.newImage :assets/icons/volume-on.png))
(tset icons :volume-off (lg.newImage :assets/icons/volume-off.png))
(tset icons :menu (lg.newImage :assets/icons/menu.png))
(tset icons :fullscreen (lg.newImage :assets/icons/fullscreen.png))

(local atlas-filename "assets/sprites/atlas.png")
(local atlas-data-filename "assets/sprites/atlas.fnl")
(set atlas.image (lg.newImage atlas-filename))


(fn parse-slice [slices]
  (let [ret {}]
    (each [name s (pairs slices)]
      (let [{: x : y : w : h} (. s :bounds)]
        (tset ret name
              {:name name
               :x (* 64 (- x 1))
               :y (* 64 (- y 1))
               :w (* 64 w)
               :h (* 64 h)})
        (when s.data
          (let [d s.data]
            (tset (. ret name) :data {})
            (each [k v (pairs d)]
              (tset (. ret name :data) k v))))
        ))
    ret))

(fn to-quads [slices image]
  (let [ret {}        
        (iw ih) (image:getDimensions)]
    (each [name s (pairs slices)]
      (let [{: x : y : w : h} s]
        (tset ret s.name (love.graphics.newQuad x y w h iw ih))))
    ret))

(fn to-brush [slices image]
  (let [ret {}        
        (iw ih) (image:getDimensions)]
    (each [name s (pairs slices)]
      (let [{: x : y : w : h : data} s]
        (tset ret s.name {: name
                          :px x
                          :py y
                          :pw w
                          :ph h
                          :iw iw
                          :ih ih
                          :w w
                          :h h})
        (when data
          (each [k v (pairs data)]
            (tset (. ret s.name) k v)))
        ))
    ret))

(set atlas.slices
     (-> atlas-data-filename
         love.filesystem.read
         ((fn [x] (fennel.eval x)))
         (. :slices)
         parse-slice
         ))

(set atlas.quads (to-quads atlas.slices atlas.image))

(set atlas.brushes (to-brush atlas.slices atlas.image))


{: fonts : atlas  : sounds : masks}
