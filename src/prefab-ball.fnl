(local {: preview : draw : add-sprite-pipeline-canvases
        : draw-water}
       (require :prefab-object))

;; (each [key {: x : y} (ipairs state.balls)]
(fn draw-callback [self image ...]
  (fn []
    (local lg love.graphics)
  (lg.push :all)
  (lg.setColor 1 1 1 1)
  ;; (pp self)
  (match self.tile
    :water
    (do
      (lg.setLineWidth 3)
      (lg.setColor 0 0 0 0.4)
      (lg.circle :line 0 0 self.radius))
    _ (do
        (when (< self.radius 20 )
          (lg.setLineWidth 3)
          (lg.setColor 0 0 0 0.4)
          (lg.circle :line (+ 0 16) (+ 0 16) self.radius)
          (lg.setColor 1 1 1 1))
        (lg.draw image self.quad)
        ))
  (lg.pop)))

(fn update [self dt]
  (set self.radius (+ self.radius (* 400 dt)))
  (when (and (= :water self.tile)
             (> self.radius 50)) (set self.time -1))
  (set self.time (- self.time dt))
  (when (< self.time 0)
    (local state (require :state))
    (set self.active false)
    (state.level:remove-obj :objects self)))

(local ball {:serialize (fn [self] self.encode)
             :handlers {}
             : preview
             : draw-water
             : update
             : draw
             : draw-callback})

(local ball-mt {:__index ball})

(fn on-tile-type [self]
  (local {: level} (require :state))
  (let [x (-> self (. :x) (+ 16) (/ 64) (math.floor) (+ 1))
        y (-> self (. :y) (+ 16) (/ 64) (math.floor) (+ 1))]
    
    (if (or (< x 1) (< y 1)) :water
        (. level.layers.ground.data (+ (* 100 (- y 1)) x) :brush))
    ))

(fn ball.create [brush]
  (pp :creating-ball)
  (local {: x : y : name} brush)
  (local {: atlas} (require :assets))
  (local assets (require :assets))
   (-> {:encode brush
        :name name
        :type :ball
        :time 5
        :active true
        :radius 0
        :tile (on-tile-type {: x : y})
        :tile-type brush.tile-type
        :quad (. atlas.quads name)
        :x brush.x :y brush.y
        :w 32 :h 32}
       ((fn [x] (setmetatable x ball-mt)))
       add-sprite-pipeline-canvases
       ))



ball
