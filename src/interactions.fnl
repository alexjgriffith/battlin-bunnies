(local list {})

(fn add [key value callback]
  (tset list key [value callback]))

(fn clear [key]
  (tset list key nil))

(fn call [key args?]
  (let [callback (?. list key 2)]
    (when callback (callback args?) true)))

{: add : clear : call}
