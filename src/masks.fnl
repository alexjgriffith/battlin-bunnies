(local assets (require :assets))

(local masks {:grass {}})
(set masks.grass.canvas (love.graphics.newCanvas 1280 720))
(set masks.grass.tileName "grass")
(set masks.grass.quads [])
;; (set masks.grass.tiles [])
(let [(iw ih) (assets.atlas.image:getDimensions)
      (qx qy) (assets.atlas.quads.TallGrassMask:getViewport)]
  (for [i 1 16]
    (local quad (love.graphics.newQuad
                 (+ qx (* 64 (math.floor (% (- i 1) 4))))
                 (+ qy (* 64 (math.floor (/ (- i 1) 4))))
                 64 64 iw ih))
    (table.insert masks.grass.quads quad)))

(fn concat-keys [t sep?]
  (var ret nil)
  (var sep (or sep? ", "))
  (each [key _ (ipairs t)]
    (if ret
        (set ret (.. ret sep key))
        (set ret key)))
  (or ret ""))

(fn make-mask [mask]
  (local state (require :state))
  (local layers [:buttons])
  (local ground :ground)
  (assert (. masks mask)
          (.. "Mask " mask " not defined. Choose from: " (concat-keys masks)))
  (tset masks mask :tiles [])
  (each  [_ tile (ipairs (. state.level.layers ground :data))]
    (when (= tile.brush (. masks mask :tileName))
      (let [{: x : y : bitmap-index} tile]
        (table.insert  (. masks mask :tiles)
                       [(-> x (- 1) (* 64)) (-> y (- 1) (* 64)) bitmap-index] ))))
  (pp masks)
  (. masks mask :canvas)
  )

(fn draw-mask [mask x y w h]
  ;; could make this into a spritebatch
  (local lg love.graphics)
  (lg.push :all)
  (lg.setCanvas (. masks mask :canvas))
  (lg.setColor 1 1 1 1)
  (lg.clear)
  (lg.rectangle :fill 0 0 100 100)
  ;; (pp (# (. masks mask :tiles)))
  (each [_ [tx ty bitmap-index] (ipairs (. masks mask :tiles))]
    (when (rect_isIntersecting x y w h tx ty 64 64)
        (lg.draw assets.atlas.image
                 (. (. masks mask :quads) bitmap-index)
                 (- tx x) (- ty y)
                 ))
  )
  (lg.setCanvas)
  (lg.pop)
  (. masks mask :canvas))


{: make-mask : draw-mask}
