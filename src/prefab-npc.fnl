(local {: draw-collider : preview : draw-shadow
        : add-collider
        : draw : draw-water : add-sprite-pipeline-canvases}
       (require :prefab-object))

(local {: atlas} (require :assets)) 

(fn distance [a b]
  (math.sqrt (+ (^ (- a.x b.x) 2) (^ (- a.y b.y) 2))))

(fn left [self]
  (local state (require :state))
  (> state.player.x self.x))

(fn open-text [self which-message?]
  (when (not which-message?)
    (local {: sounds} (require :assets))
    (sounds.page:stop)
    (sounds.page:play))
  (set self.text.timer 0)
  (set self.text.count 0)
  (set self.text.which-message (or which-message? 1))
  (tset self.text :active true))

(fn close-text [self]
  (local {: sounds} (require :assets))
  (sounds.page:stop)
  (sounds.page:play)
  (tset self.text :active false)
  (when self.game-over
    (local {: fade-out-switch} (require :transition))
    ((fade-out-switch :mode-ending))
    )
  )

(fn update-facing [self dt d])

(fn interaction-callback [self]
  (fn [] (open-text self)))

(fn update-prompt [self dt d]
  (local interactions (require :interactions))
  (if (and (not self.text.active) (< d (* 64 self.interaction-radius)))
      (do (tset self :prompt true)
          (interactions.add :i d (interaction-callback self)))
      (tset self :prompt false)))

(fn next-text-callback [self]
  (fn []
    (let [t self.text
          last-message (= t.which-message (# (. t.message self.game-state)))]
      (if last-message
          (close-text self)
          (open-text self (+ t.which-message 1))))))

(fn close-text-callback [self]
  (fn [] (close-text self)))

(fn update-text [self dt d]
  (local interactions (require :interactions))
  (local speed 0.02)
  (local message (. self.text.message self.game-state self.text.which-message))
  ;; out of range close
  (when (and (> d (* 4 64)) self.text.active) (close-text self))
  (when self.text.active
    (if (< self.text.count (# message))
      (do (set self.text.timer (+ self.text.timer dt))
          (local next-count (math.floor (/ self.text.timer speed)))
          (when (and (~= next-count self.text.count) (~= (string.sub message next-count next-count) " "))
            (local {: sounds} (require :assets))
            (sounds.talk:stop)
            (sounds.talk:play))
          (set self.text.count next-count)
          (interactions.add :i d (fn [] (set self.text.count (# message)))))
      (interactions.add :i d (next-text-callback self)))
    (interactions.add :escape d (close-text-callback self)))
  )

(fn draw-text [self image]
  (when self.text.active
    (let [lg love.graphics
          text-box (require :text-box)
          vink (require :lib.vink24)
          w (* 64 8)
          h (* 64 3)
          xp (- self.x (/ w 2) -32)
          y (- self.y h)
          x (if false (+ xp 64) xp)
          message (. self.text.message self.game-state self.text.which-message)
          {: fonts} (require :assets)
          ]
      (lg.push)
      (lg.setLineWidth 1)
      (lg.translate x y)
      (lg.setColor 1 1 1 1)
      (lg.rectangle :fill 0 0 w h 5)
      (lg.setColor 0 0 0 1)
      (lg.rectangle :line 0 0 w h 5)
      (lg.translate 5 5)
      (text-box.test-wrap (- w 10) self.text.count message)
      (lg.setFont fonts.small-text)
      (local last-message (= self.text.which-message (# (. self.text.message self.game-state))))
      (local message-complete (= (# (. self.text.message self.game-state self.text.which-message))
                                 self.text.count))
      (when message-complete
        (if last-message
            (lg.printf "Press I to Close" 5 (- h 40) (- w 10) :center)
            (lg.printf "Press I to Continue" 5 (- h 40) (- w 10) :center)))
      ;; (lg.print "Press I to Close")
      (lg.pop))))

(fn draw-interaction-prompt [self image]
  (when (and self.prompt (not self.text.active))
    (let [lg love.graphics
          {: fonts} (require :assets)
          message "Press I to interact!"
          ntext (lg.newText fonts.small-text message)
          (w h) (values (ntext:getWidth) (ntext:getHeight))
          vink (require :lib.vink24)
          xp (- self.x (/ w 2) -32)
          x (if false (+ xp 64) xp)
          y (- self.y h)
          ]
      (lg.push)
      (lg.setLineWidth 1)
      (lg.setColor vink.purple-4)
      (lg.setFont fonts.small-text)
      (lg.draw ntext x y )
      (lg.pop))))


(fn draw-callback [self image ...]
  (fn []
    (local lg love.graphics)
    (lg.push)
    (lg.setColor 1 1 1 1)
    (lg.draw image self.quad (if false 64 0) 0 0 (if false -1 1) 1)
    (lg.pop)
    ))

(fn get-state [self]
  (local state (require :state))
  (match (# state.player.bunnies)
    0 :nobunnies
    5 :allbunnies
    _ :bunnies
    ))

(fn check-game-over [self]
  (and (= self.name :Ben)
       (= self.game-state :allbunnies)
       (= self.text.which-message (# (. self.text.message :allbunnies)))
       ))

(fn update-game-state [self]
  (set self.game-over (check-game-over self))
  (local next-state (get-state self))
  (when (and (~= next-state self.game-state) self.text.active) (close-text))
  (set self.game-state next-state))

(local npc {:serialize (fn [x] x.encode)
               : draw
               : draw-callback
               : draw-water
            :update (fn [self dt]
                      (let [state (require :state)
                            d (distance self state.player)]
                        (tset self :left (left self))
                        (update-game-state self dt d)
                        (update-facing self dt d)
                        (update-prompt self dt d)
                        (update-text self dt d)))
               : preview
               : draw-collider
            : draw-text
            : draw-shadow            
            : draw-interaction-prompt
               :handlers {}               
               })

(local npc-mt {:__index npc})

(local npc-text (require :npc-text))

(fn npc.create [brush]
  (local {: px : py : pw : ph : iw : ih} brush)
  (-> {:encode brush
       :mouse {:active false :x 0 :y 0 :right-down false :left-down false :cx 0 :cy 0}
       :name brush.name
       :quad (love.graphics.newQuad px py pw ph iw ih)
       :state :idle
       :shadow brush.shadow
       :shadow-quad (?. atlas.quads brush.shadow)
       :sy brush.sy
       :sx brush.sx
       :game-state :nobunnies
       :text {:active false
              :message (or (. npc-text brush.name) {:nobunnies ["Missing"]
                                                    :bunnies ["Missing"]
                                                    :allbunnies ["Missing"]
                                                    })
              :which-message 1
              :timer 0
              :count 0}
       :prompt true
       :interaction-radius 2
       :left false
       :x brush.x :y brush.y
       :w brush.w :h brush.h}
      ((fn [x] (setmetatable x npc-mt) x))
      add-sprite-pipeline-canvases
      add-collider))

npc
