(local {: draw-collider : preview : draw-shadow
        : add-collider
        : draw : draw-water : add-sprite-pipeline-canvases}
       (require :prefab-object))

(local {: atlas} (require :assets)) 
 
(local speed 400)
(local dash-speed 2000)
(local crouch-speed 200)
(local dash-distance (* 64 4))
(var dash false)
(var dash-down false)
(var dash-timer 0)
(var dash-vector {:vx 0 :vy 0})


(fn draw-callback [self image ...]
  (fn []
    (local lg love.graphics)
    (lg.push)
    (lg.setColor 1 1 1 self.alpha)
    
    (lg.draw image self.quad (if true 64 0) 0 0 (if true -1 1) 1)
    (lg.pop)
    ))

(fn on-tile-type [self]
  (local {: level} (require :state))
  (let [x (-> self (. :x) (+ 32) (/ 64) (math.floor) (+ 1))
        y (-> self (. :y) (+ 128) (/ 64) (math.floor) (+ 1))]
    (. level.layers.ground.data (+ (* 100 (- y 1)) x) :brush)))

(fn mouse-move [self dt]
  (let [{ : right-down : left-down} self.mouse
        (mx my) (love.mouse.getPosition)
        focused (love.window.hasMouseFocus)
        right-click (love.mouse.isDown 1)
        left-click (love.mouse.isDown 2)]
    (tset self.mouse :active focused)
    (match [right-click right-down]
      [true false] (do ;; (pp [right-click right-down]) ;; trigger-on
                       (tset self.mouse :cx mx)
                       (tset self.mouse :cy my)
                       (love.mouse.setGrabbed true)
                       )
      [false true] (do ;; (pp [right-click right-down]) ;; trigger-off
                     (love.mouse.setGrabbed false)
                     (set self.throw.release true)
                     ))
    (tset self.mouse :right-down right-click)))

(fn quad-bezier [t p1x p1y p2x p2y p3x p3y]
  (let [a (^ (- 1 t) 2)
        b (* 2 t (- 1 t))
        c (^ t 2)]
    (values (+ (* a p1x) (* b p2x) (* c p3x)) (+ (* a p1y) (* b p2y) (* c p3y)))
    ))

(fn quad-bezier-line [array steps p1x p1y p2x p2y p3x p3y]
  (for [i 0 steps]
    (let [t (/ i steps)
          (x y) (quad-bezier t p1x p1y p2x p2y p3x p3y)]
      (tset array (+ (* (- i 0) 2) 1) x)
      (tset array (+ (* (- i 0) 2) 2) y)))
  (tset array (+ (* (- steps 0) 2) 3) nil) ;; terminate the array
  array)

(fn draw-odd-points [array]
  (for [i 1 (# array) 6]
  (love.graphics.line (. array (+ i 0))
           (. array (+ i 1))
           (. array (+ i 2))
           (. array (+ i 3)))))

(local __barray []) ;; this array is reused
(fn draw-arch [self]
  (local lg love.graphics)
  (let [(mx my) (love.mouse.getPosition)
        max (* 3 64)
        scale 2
        dxp (- self.mouse.cx mx) 
        dyp (- self.mouse.cy my)
        mag (math.sqrt (+ (^ dxp 2) (^ dyp 2)))
        ux (if (~= 0 mag) (/ dxp mag) 0)
        uy (if (~= 0 mag) (/ dyp mag) 0)
        magn (math.min mag max)
        dx (* ux magn scale)
        dy (* uy magn scale)
        crouch (= self.state :crouch)
        ox 58
        oy1 (if crouch 80 40)
        oy2 (if crouch 60 20)
        oy (+ oy1 (* (/ magn max) (- oy1 oy2)))
        p1x (+ self.x ox)
        p1y (+ self.y oy)        
        p2x (+ self.x (/ dx 2) ox)
        p2y (+ self.y (/ dy 2) oy (* (/ magn max) -256))
        p3x (+ self.x dx ox)
        p3y (+ self.y dy oy)
        array (if self.throw.release
                  __barray
                  (quad-bezier-line __barray 100 p1x p1y p2x p2y p3x p3y))
        quad (if (= self.throw.what :ball) atlas.quads.SmallBall atlas.quads.SmallBone)
        ]
    (lg.push :all)
    (when (and self.mouse.right-down (not self.throw.release))
      (lg.setLineWidth 1)
      (local vink (require :lib.vink24))
      (lg.setColor 1 1 1 1)
      (lg.ellipse :fill (+ self.x ox) (+ self.y oy) 4 6)
      (lg.setColor 0 0 0  1)
      (lg.ellipse :line (+ self.x ox) (+ self.y oy) 4 6)
      (lg.ellipse :line (+ self.x ox) (+ self.y oy) 5 6)
      (lg.setColor 1 1 1 1)
      (lg.draw atlas.image quad (+ self.x ox -12) (+ self.y oy -22))
      (lg.setLineWidth 3)
      (lg.setColor vink.red-1)
      (draw-odd-points array)
      (lg.setLineWidth 2)
      (lg.ellipse :line (+ self.x dx ox) (+ self.y dy oy) 24 16)
      )
    (when self.throw.release
      (local i self.throw.index)
      (local x (. array (-> i (- 1) (* 2) (+ 1))))
      (local y (. array (-> i (- 1) (* 2) (+ 2))))
      (lg.draw atlas.image quad x y)
      (set self.throw.index (+ self.throw.index 5))
      (when (> self.throw.index (/ (# self.throw.array) 2))
        (love.event.push :ball-land x y self.throw.what)
        (set self.throw.index 1)
        (set self.throw.release false))
      )
    
    (lg.pop))
  )

(fn player-move [self dt]
  (let [isDown love.keyboard.isDown
        vyp (+ (if (isDown :down :s) 1 0) (if (isDown :up :w) -1 0))
        vxp (+ (if (isDown :left :a) -1 0) (if (isDown :right :d) 1 0))
        mag (math.sqrt (+ (^ vyp 2) (^ vxp 2)))
        dash-active (or (and (~= 0 mag) (isDown :space) (not dash-down)) dash)
        crouch (isDown :lshift :rshift)
        s (* (if dash-active dash-speed crouch crouch-speed speed) dt)
        vy (if dash dash-vector.vy (= mag 0) 0 (-> vyp (/ mag) (* s)))
        vx (if dash dash-vector.vx (= mag 0) 0 (-> vxp (/ mag) (* s)))
        x (+ self.x self.encode.cx vx)
        y (+ self.y self.encode.cy vy)
        {: world} (require :state)
        (ax ay col) (world:move self x y (fn [item other]
                                           (if (or other.cross
                                                   (= other.type :bunny))
                                               :cross
                                               :slide)
                                           ))
        ]
    (tset self :x (- ax self.encode.cx))
    (tset self :y (- ay self.encode.cy))
    (when (and dash-active (not dash))
      (set dash-down true)
      (set dash-vector {: vx : vy})
      (set dash (- dash-distance (+ (math.abs vx) (math.abs vy)))))
    (when dash
      (set dash (- dash (+ (math.abs vx) (math.abs vy))))
      (when (< dash 0)
        (set dash false)))
    (when (or (not (isDown :space)) (not (isDown :up :w :down :s :left :a :right :d)))
      (set dash-down false))
    (set self.state
         (match [dash crouch]
           [false false] :idle
           [false true] :crouch
           [_ _] :dash))
    (values col vy vx)
    )
  )

(fn switch-throw [self]
  (if (= self.throw.what :ball)
      (set self.throw.what :food)
      (set self.throw.what :ball))
  self)

(fn euclidean-distance [x1 y1 x2 y2]
  (math.sqrt (+ (^ (- x1 x2) 2) (^ (- y1 y2) 2))))

(local player {:serialize (fn [x] x.encode)
               : draw
               : draw-callback
               : draw-water
               :update (fn [self dt]
                         (local (col vx vy dash-active) (player-move self dt))
                         (local player-tile (on-tile-type self))
                         (if (= self.state :crouch)
                             (set self.quad self.crouch-quad)
                             (set self.quad self.stand-quad))
                         (if (and (= self.state :crouch) (= player-tile :grass))
                             (do (set self.alpha 0.5)
                                 (set self.hidden true))
                             (do (set self.alpha 1)
                                 (set self.hidden false)))
                         (table.sort self.bunnies (fn [a b]
                                                    (< (euclidean-distance self.x (+ 48 self.y)
                                                                           a.x a.y)
                                                       (euclidean-distance self.x (+ 48 self.y)
                                                                           b.x b.y))
                                                    ))
                         (mouse-move self dt))
               : preview
               : draw-collider
               : draw-arch
               : draw-shadow
               : switch-throw
               :handlers {}               
               })

(local player-mt {:__index player})

(fn player.create [brush]
  (local {: px : py : pw : ph : iw : ih} brush)
  (-> {:encode brush
       :mouse {:active false :x 0 :y 0 :right-down false :left-down false :cx 0 :cy 0}
       :name :Player
       :shadow brush.shadow
       :shadow-quad (?. atlas.quads brush.shadow)
       :sy brush.sy
       :sx brush.sx
       :stand-quad (love.graphics.newQuad px py pw ph iw ih)
       :crouch-quad atlas.quads.PlayerCrouch
       :state :idle
       :depth 20
       :alpha 1
       :hidden false
       :bunnies []
       :throw {:release false :index 1 :array __barray :what :ball}
       :x brush.x :y brush.y
       :w brush.w :h brush.h}
      ((fn [x] (setmetatable x player-mt) x))
      add-sprite-pipeline-canvases
      add-collider))


player
