(local {: draw-collider : preview : draw-shadow
        : add-collider
        : draw-water : add-sprite-pipeline-canvases}
       (require :prefab-object))

(local lg love.graphics)
(local vink (require :lib.vink24))
(local {: fonts : atlas} (require :assets))
(local gizmo (require :gizmo))

(local {: atlas} (require :assets)) 

(local {: noise-array} (require :simplex-noise))
(local noise (noise-array 0.5 2048))

(fn debug-draw [self]
  (local max-distance self.max-distance)
  (local t-voids self.t-voids)
  (local f-voids self.f-voids)
  (lg.setColor 1 1 1 1)
  (local {: fonts } (require :assets))
  (lg.setFont fonts.text)
  (gizmo.draw-direction-gizmo
   self
   (icollect [_ w (ipairs self.weights)] (* w 100))
   (icollect [_ w (ipairs self.void-weights)] (* w 100))
   self.direction-index)
  (lg.circle :line (+ self.x 32) (+ self.y 32) self.max-distance)
  (lg.circle :fill self.tx self.ty 40)

  (lg.circle :line self.home-x self.home-y (* 2 64))
  (lg.circle :line self.home-x self.home-y (* 4 64))
  (each [key {: x : y} (ipairs t-voids)]
    (lg.circle :fill x y 10))
  (lg.setColor 1 0 0 1)
  (each [key {: x : y} (ipairs f-voids)]
    (lg.circle :fill x y 10)))

(fn draw-emoji [self]
  (lg.push)
  (when self.emoji
    (lg.draw atlas.image (. atlas.quads self.emoji)
             self.x (- self.y 50)))
  (local {: fonts} (require :assets))
  ;; (lg.setFont fonts.small-text)
  ;; (lg.printf self.state (- self.x 64) (+ self.y 60) (+ 64 128) :center)
  (lg.pop))

(fn next-mode [self]
  (var mode-index 1)
  (each [key value (ipairs self.mode-order)]
    (when (= value self.current-mode)
      (set mode-index key)))
  (set self.current-mode (. self.mode-order (+ (% (- (+ mode-index 1) 1) (# self.mode-order)) 1))))

(fn draw-callback [self image ...]
  (fn []
    (local lg love.graphics)
    (lg.push)
    (lg.setColor 1 1 1 1)
    (when (~= self.state :hide)
      (lg.draw image self.quad (if false 64 0) 0 0 (if false -1 1) 1))
    (lg.pop)
    ))

(fn draw [self image ...]
  (local {: grass-shader-pipeline} (require :prefab-object))
  (grass-shader-pipeline self (self.draw-callback self image) 0 0))

(fn draw-home [self image ...]
  (local lg love.graphics)
  (lg.push)
  (lg.setColor 1 1 1 1)
  (lg.translate self.home-x self.home-y)
  (lg.draw image atlas.quads.Hole )
  (lg.pop))

(fn vector->angle [x1 y1 x2 y2]
  (math.atan2 (- y2 y1) (- x2 x1))
  )

(fn euclidean-distance [x1 y1 x2 y2]
  (math.sqrt (+ (^ (- x1 x2) 2) (^ (- y1 y2) 2))))

(fn index-to-vector  [index velocity distance-to-target angle-count speed]
  (let [angle (* (- index 1) (/ (* 2 math.pi) angle-count))
        radius (if (< distance-to-target (* speed 4)) (/ distance-to-target  (* speed 4)) 1)
        x (* (math.cos angle) radius)
        y (* (math.sin angle) radius)]
    (values x y)))

;; states idle, alert, aware, follow, target

(fn threatened [self]
  (local {: player} (require :state))
  (local detection-distance (* 64 6))
  (local distance
         (euclidean-distance
          (+ 32 self.x) (+ self.y 32)
          (+ player.x 32) (+ player.y 64)))
  (if (and (< distance detection-distance)
           (not self.tame)
           (not player.hidden))
      true
      false))

(fn get-food-and-ball [self]
  (local state (require :state))
  (local balls state.balls)
  (local max-distance (* 64 4))
  (when (and self.ball (not self.ball.active))
    (set self.ball nil))
  (when (and self.food (not self.food.active))
    (set self.food nil))
  (var ball-dist max-distance)
  (var food-dist max-distance)
  (var ball-index nil)
  (var food-index nil)  
  (each [key value (ipairs balls)]
    (local distance (euclidean-distance (+ self.x 32) (+ self.y 32) value.x value.y))
    (match value.name
      :SmallBall (when (< distance ball-dist)
                     (set ball-dist distance) (set ball-index key))
      :SmallBone (when (< distance food-dist)
                     (set food-dist distance) (set food-index key))))
  ;; (pp food-index)
  (when (and (not self.ball) ball-index)
    (set self.ball (. balls ball-index)))
  (when (and (not self.food) food-index)
    (set self.food (. balls food-index)))
  )

(fn update-follow [self dt]
  (local {: player} (require :state))
  (local (mx my) (match self.index
                   1 (values (+ 32 player.x) (+ player.y 32 48))
                   _ (let [b (. player.bunnies (- self.index 1))]
                       (values (+ 32 b.x) (+ b.y 32)))))
  (tset self :current-mode :follow)
  (tset self :speed 5)
  (tset self :tx mx)
  (tset self :ty my))

(fn update-idle [self dt]
  (let [angle1 (* 2 math.pi (. noise self.noise-index))
        distance (euclidean-distance self.x self.y self.home-x self.home-y)
        angle2 (vector->angle self.x self.y self.home-x self.home-y)
        max 4
        min 2
        weight (math.min 1 (math.max 0 (/ (- (/ distance 64) min) max)))
        angle (+ (* weight angle2) (* (- 1 weight) angle1))
        mx1 (+  (* (- 1 weight) 100 (math.cos angle1)))
        my1 (+  (* (- 1 weight) 100 (math.sin angle1)))
        mx2 (+  (* weight 100 (math.cos angle2)))
        my2 (+   (* weight 100 (math.sin angle2)))
        mx (+ self.x 32 mx1 mx2)
        my (+ self.y 32 my1 my2)
        ]
    (tset self :speed 2)
    (tset self :noise-index (+ (% self.noise-index (# noise)) 1))
    (tset self :current-mode :chase)
    (tset self :tx mx)
    (tset self :ty my)
    (set self.time (- self.time dt))
    (when (and (not self.tame) self.food)
      (set self.time 0.5)
      (set self.state :see-food))
    (when (and self.tame self.ball)
      (set self.time 0.5)
      (set self.state :see-ball))
    (when (and (< self.time 0) (threatened self))
      (set self.time 1)
      (set self.state :alert))
    ))

(fn update-alert [self dt]
  (tset self :current-mode :chase)
  (tset self :speed 0)
  (tset self :tx self.x)
  (tset self :ty self.y)
  (tset self :emoji :Exclaim)
  (set self.time (- self.time dt))
  (when (< self.time 0)
    ;; (tset self :emoji nil)
    (if (threatened self)
        (set self.state :run-home)
        (do (set self.state :idle)
            (tset self :emoji nil)))))

(fn update-run-home [self dt]
  (local (mx my) (values (+ self.home-x 32) (+ self.home-y 32)))
  (tset self :current-mode :chase)
  (tset self :speed 10)
  (tset self :tx mx)
  (tset self :ty my)
  (when (< (euclidean-distance (+ self.x 32) (+ self.y 32) self.tx self.ty) 32)
    (tset self :emoji nil)
    (if (threatened self)
        (do (set self.time 5)
            (set self.state :hide))
        (do
          (set self.time 0.5)
          (set self.state :alert))
        )))

(fn update-hide [self dt]
  (tset self :current-mode :chase)
  (tset self :speed 0)
  (tset self :tx self.x)
  (tset self :ty self.y)
  (set self.time (- self.time dt))
  (when (< self.time 0)
    (set self.time 1)
    (set self.state :idle)))

(fn update-see-food [self dt]
  (tset self :current-mode :chase)
  (tset self :speed 5)
  (when self.food
      (tset self :tx self.food.x)
      (tset self :ty self.food.y))
  (tset self :emoji :Question)
  (set self.time (- self.time dt))
  (when (< self.time 0)
    (tset self :emoji nil))
  (when (< (euclidean-distance (+ self.x 32) (+ self.y 32) self.tx self.ty) 32)
    (when self.food (set self.food.time -1))
    (set self.time 1)
    (tset self :emoji nil)
    (local {: sounds} (require :assets))
    (sounds.crunch:stop)
    (sounds.crunch:play)
    (set self.state :eat-food))
  (when (threatened self)
      (set self.time 1)
      (set self.state :alert))
  (when (or (not self.food) (not self.food.active))
    (tset self :emoji nil)
    (set self.state :idle)))

(fn update-eat-food [self dt]
  (tset self :current-mode :chase)
  (tset self :speed 0)
  (tset self :tx self.x)
  (tset self :ty self.y)
  (set self.time (- self.time dt))
  (when (threatened self)
    (set self.time 1)
    (set self.state :alert))
  (when (< self.time 0)
    (set self.eaten (+ self.eaten 1))
    (when (>= self.eaten 5)
      (set self.tame true))
    (set self.state :idle)))

(fn update-see-ball [self dt]
  (tset self :current-mode :chase)
  (tset self :speed 5)
  (when self.ball
      (tset self :tx self.ball.x)
      (tset self :ty self.ball.y))
  (tset self :emoji :Smile)
  (set self.time (- self.time dt))
  (when (< self.time 0)
    (tset self :emoji nil))
  (when (< (euclidean-distance (- self.x 0) (- self.y 0) self.tx self.ty) 48)
    (when self.ball (set self.ball.time -1))
    (local {: sounds} (require :assets))
    (sounds.land:stop)
    (sounds.land:play)
    (tset self :emoji nil)
    (set self.state :bring-ball))
  (when (or (not self.ball) (not self.ball.active))
    (tset self :emoji nil)    
    (set self.state :idle)))


(fn update-bring-ball [self dt]
  (local {: player} (require :state))
  (local (mx my) (values (+ 32 player.x) (+ player.y 32 48)))
  (tset self :current-mode :chase)
  (tset self :speed 5)
  (tset self :tx mx)
  (tset self :ty my)
  (when (< (euclidean-distance (+ self.x 0) (+ self.y 0) self.tx self.ty) 64)
    (set self.caught (+ self.caught 1))
    (when (>= self.caught 5)
      (when (not self.friend)
        (table.insert player.bunnies self)
        (tset self :index (# player.bunnies))
        (set self.state :follow))
      (set self.friend true))
    (when (not (= self.state :follow))
      (set self.state :idle))))

(fn update [self dt]
  (local directional (require :directional))
  (local state (require :state))
  (get-food-and-ball self)
  ;; (when self.food (pp self.food))
  (match self.state
    :follow (update-follow self dt)
    :idle (update-idle self dt)
    :alert (update-alert self dt)
    :run-home (update-run-home self dt)
    :hide (update-hide self dt)
    :see-food (update-see-food self dt)
    :eat-food (update-eat-food self dt)
    :see-ball (update-see-ball self dt)
    :bring-ball (update-bring-ball self dt)
    _ :nothing)
  ;;(local (mx my) (values (+ 32 state.player.x) (+ state.player.y 32 48)))
  ;; (local (mx my) (love.mouse.getPosition))
  (local (mx my) (values self.tx self.ty))
  (local max-distance self.max-distance) ;; (* 1.5 128)
  (local sense-distance self.sense-distance) ;;(* 10 128)
  (local voids (state.world:queryRect
                (- self.x -32 max-distance)
                (- self.y -32 max-distance)
                (* max-distance 2)
                (* max-distance 2)
                (fn [item] (if (not self.tame)
                               (and (~= item.name self.name) (~= item.type :bunny))
                               (and (~= item.name self.name)
                                    (~= item.type :bunny)
                                    (~= item.name :Player)
                                    (not item.cross)))
                  )))
  (local t-voids [])
  (each [i v (ipairs voids)]
    (local (x y w h) (state.world:getRect v))
    (tset t-voids i {:x (+ x (/ w 2)) :y (+ y (/ h 2))}))
  (set self.t-voids t-voids)
  (local f-voids [])
  (each [i {: x : y &as v} (ipairs t-voids)]
    (when (> max-distance (euclidean-distance x y (+ self.x 32) (+ self.y 32)))
      (table.insert f-voids v)))
  (set self.f-voids f-voids)
  (local within-sense-distance (> sense-distance (euclidean-distance mx my
                                                                     (- self.x 32)
                                                                     (- self.y 32))))
  (local (move index weights void-weights)
         (directional.update
          self.x self.y
          {:x (- (if within-sense-distance mx self.x) 32)
           :y (- (if within-sense-distance my self.y) 32)}
          f-voids
          (. self.modes self.current-mode)
          (fn [ux uy _distance]
            (values ux uy))
          max-distance))
  (set self.weights weights)
  (set self.void-weights void-weights)
  (set self.direction-index index)
  ;; bounce
  (when (< (. weights index) (+ 0.6 (* 0.3 (math.random))))
    (set self.left (not self.left)))
  ;; move
  (let [speed (* dt 60 self.speed)
        (x y) (index-to-vector index (. weights index)
                               (euclidean-distance mx my (+ self.x 32) (+ self.y 32))
                               (# weights)
                               speed)]
    (when move
      (let [tx (+ (+ self.x self.cx) (* speed x))
            ty (+ (+ self.y self.cy) (* speed y))
            (ax ay cols len)
            (state.world:move self tx ty
                              (fn [item other]
                                (if (or (= other.type :bunny)
                                        (= other.name :Player)
                                        other.cross)
                                    :cross
                                    :slide)))]
        (tset self :x (- ax self.cx ))
        (tset self :y (- ay self.cy ))
        ;; (tset self :x (- tx self.cx))
        ;; (tset self :y (- ty self.cy))
        (when (> len 0) (set self.left (not self.left)))
        ))
    )
  )


(local bunny {:serialize (fn [x] x.encode)
              : draw-emoji              
              : draw
              : draw-callback
              : draw-water
              : draw-home
              : update
              : preview
              : debug-draw
              : next-mode
              : draw-shadow
            :handlers {}               
            })

(local bunny-mt {:__index bunny})

(fn circle [min max tableleft?]
  (fn [ux uy distance]
    (if (< distance min)
        ;;(values ux uy)
        (values (- ux) (- uy))
        (> distance max)
        (values ux uy)
        (if (and tableleft? tableleft?.left)
            (values (+ uy) (- ux))
            (values (- uy) (+ ux)))
        )))

(fn follow [min max]
  (fn [ux uy distance]
    (local delta (/ (- distance min) (- max min)))
    (if
     (> distance max)
     (values ux uy)
     (< distance min)
     (values 0 0)
     (values (* ux delta) (* uy delta))
     )))


(fn chase []
  (fn [ux uy distance]
    (values ux uy)))

(fn flee []
  (fn [ux uy distance]
    (values (- ux) (- uy))))


(fn bunny.create [brush]
  (local {: px : py : pw : ph : iw : ih} brush)
  (local bunny
         (-> {:encode brush
              :name brush.name
              :quad (love.graphics.newQuad px py pw ph iw ih)
              :max-distance (* 1.5 128)
              :sense-distance (* 10 128)
              :quad (. atlas.quads brush.name)
              :shadow brush.shadow
              :shadow-quad (?. atlas.quads brush.shadow)
              :sy brush.sy
              :sx brush.sx              
              :type :bunny
              :cx brush.cx
              :cy brush.cy
              :cw brush.cw
              :ch brush.ch
              :home-x brush.x
              :home-y brush.y
              :tx brush.x
              :ty brush.y
              :tame false
              :friend false
              :dx 0
              :dy 0
              :state :idle
              ;; :canvas-offset {:x -32 :y -32 :w 64 :h 64}
              :speed 5
              :time 0
              :depth 20
              :weights []
              :void-weights []
              :t-voids []
              :f-voids []
              :direction-index 1
              :x brush.x :y brush.y
              :w brush.w :h brush.h
              :noise-index (math.random 1 (# noise))
              :face-left false
              :tame false
              :friend false
              :eaten 0
              :caught 0
              :left true :current-mode :follow
              :mode-order [:chase :circle :flee :follow]
              }
             ((fn [x] (setmetatable x bunny-mt) x))
             add-sprite-pipeline-canvases
             add-collider))
  (tset bunny :modes {:chase (chase) :circle (circle 200 300 bunny) :flee (flee)
                      :follow (follow 48 96)})
  bunny)


bunny
