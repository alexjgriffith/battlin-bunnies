(local repl (require :lib.stdio))

(local gamestate (require :lib.gamestate))
(local fade (require :lib.fade))
(local flux (require :lib.flux))
(local vink (require :lib.vink24))

(fn love.load [args]
  (if (= :dev (. args 1)) (global dev true) (global dev false))
  (if (= :web (. args 1)) (global web true) (global web false))
  (love.window.setTitle (.. "Bunny Buddies - by alexjgriffith" (if dev " - DEV" "")))
  (require :handlers)
  (love.graphics.setBackgroundColor vink.purple-4)
  (local {: sounds} (require :assets))
  (sounds.bgm:play)
  (gamestate.registerEvents)
  (gamestate.switch (require (if dev :mode-intro :mode-intro)))
  (when dev (repl.start)))

;; global key bindings
(fn love.keypressed [key]
  (local {: fade-out-switch : fade-out-push } (require :transition))
  (if (and (love.keyboard.isDown "lctrl" "rctrl" "capslock") (= key "q"))
      (love.event.quit))
  (if (and (love.keyboard.isDown "lctrl" "rctrl" "capslock") (= key "v"))
      (do
        ((fade-out-push :mode-ink))))
  (if (and (love.keyboard.isDown "lctrl" "rctrl" "capslock") (= key "g"))
      ((fade-out-push :mode-gizmo)))
  (if (and (love.keyboard.isDown "lctrl" "rctrl" "capslock") (= key "p"))
      (love.graphics.captureScreenshot "screenshot.png"))
  (if (and (love.keyboard.isDown "lctrl" "rctrl" "capslock") (= key "e"))
      (let [state (require :state)]
        (when (and state  (= "boolean" (type state.editing)))
          (tset state :editing (not state.editing))))))

;; updates required for all modes
(fn love.update [dt]
  (local {: sounds} (require :assets))
  (local state (require :state))
  (local handlers (require :handlers))
  (sounds.bgm:setVolume 0.2)
  ;; (sounds.bgm:setVolume (* state.volume (math.min 1 (fade.alpha))))
  (handlers.clear-hover)
  (flux.update dt)
  (fade.update (math.min dt 0.032)))

(fn love.mousefocus [f])

(fn love.mousereleased [button x y]
  (local handlers (require :handlers))
  (handlers.clear-click))
