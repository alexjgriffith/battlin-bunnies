(local lg love.graphics)
(local vink (require :lib.vink24))
(local {: fonts : atlas} (require :assets))
(local gamestate (require :lib.gamestate))
(local {: fade-pop } (require :transition))
(local fade (require :lib.fade))

(fn draw-ink-test [ink]
  (local w 256)
  (var i 1)
  (lg.push)
  (each [name color (pairs ink)]
    (lg.push :all)
    (lg.setColor color)
    (lg.rectangle :fill 0 0 w 64)
    (lg.pop)
    (local value (+ (. color 1) (. color 2) (. color 3)))
    (lg.setFont fonts.sub-title)
    (if (> value 1.5)
        (lg.setColor 0 0 0 1)
        (lg.setColor 1 1 1 1))
    (lg.printf name 0 0 w :center)
    (lg.translate w 0)
    (when (= 0 (% i 5)) (lg.translate (* -5 w) 64))      
    (set i (+ i 1)))
  (lg.pop))

{:enter (fn enter [obj] (fade.in))
 :draw (fn draw [obj]
         (draw-ink-test vink)
         (lg.draw fade.canvas))
 :update (fn update [obj dt set-mode])
 :keypressed (fn keypressed [gstate key set-mode]
               (when (not (love.keyboard.isDown "lctrl" "rctrl" "capslock"))
                 (match key :escape ((fade-pop)))))
 }
