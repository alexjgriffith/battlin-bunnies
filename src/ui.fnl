(local lg love.graphics)

(fn handle-mouse [click over key callback]
  (when over (love.event.push :over key))
  (when (and over click)
    (local {: callbacks} (require :handlers))
    (tset callbacks key callback)
    (love.event.push :click key)))

(fn get-mouse-info []
  (local (sw sh) (love.window.getMode))
  (local (mx my) (love.mouse.getPosition))
  (local click (love.mouse.isDown 1))
  {: sw : sh : mx : my : click})

(fn button [{: sw : sh : mx : my : click} text oh callback]
  (local vink (require :lib.vink24))
  (local {: fonts} (require :assets))
  (local (w h) (values 128 64))
  (local (x y) (values (/ (- sw w) 2) oh))
  (local over (pointWithin mx my x y w h ))
  (local {: fonts} (require :assets))
  (local vink (require :lib.vink24))
  (lg.push :all)
  (lg.translate x y)
  (if (not over)
      (do
        (lg.setColor vink.purple-4)
        (lg.rectangle :fill 0 0 w h 10)
        (lg.setColor vink.beige-1)
        (lg.rectangle :line 0 0 w h 10))
      (do  
        (lg.setColor vink.beige-1)
        (lg.rectangle :line 0 0 w h 10)
        (lg.rectangle :fill 0 0 w h 10)
        (lg.setColor vink.purple-4)))
  (lg.setFont fonts.text)
  (lg.printf text 0 10 w :center)
  (handle-mouse click over (.. text oh) callback)
  (lg.pop))

(fn click-text [{: sw : sh : mx : my : click} text x y w position callback]
  (local vink (require :lib.vink24))
  (local {: fonts} (require :assets))
  (local ntext (lg.newText fonts.text text))
  (local (w h) (values (ntext:getWidth) (ntext:getHeight)))
  (local over (pointWithin mx my x y w h ))
  (local {: fonts} (require :assets))
  (local vink (require :lib.vink24))
  (lg.push :all)
  (lg.translate x y)
  (if (not over)
      (lg.setColor vink.beige-1)
      (do  
        (lg.setColor vink.beige-1)
        (lg.rectangle :line 0 0 w h 10)
        (lg.rectangle :fill 0 0 w h 10)
        (lg.setColor vink.purple-4)))
  (lg.setFont fonts.text)
  (lg.printf text 0 0 w position)
  (handle-mouse click over (.. text x y) callback)
  (lg.pop))

(fn draw-credit [{: sw : sh : mx : my : click} x y {: title : author : modified : licence : link}]
  (local vink (require :lib.vink24))
  (local {: fonts} (require :assets))
  (local (w h) (values (/ (- 1280 60) 2) 165))
  (local over (pointWithin mx my x y w h ))
  (local {: fonts} (require :assets))
  (local vink (require :lib.vink24))
  (lg.push :all)
  (lg.translate x y)
  (if (not over)
      (do
        (lg.setColor vink.purple-4)
        (lg.rectangle :fill 0 0 w h 10)
        (lg.setColor vink.beige-1)
        (lg.rectangle :line 0 0 w h 10))
      (do  
        (lg.setColor vink.beige-1)
        (lg.rectangle :line 0 0 w h 10)
        (lg.rectangle :fill 0 0 w h 10)
        (lg.setColor vink.purple-4)))
  (local text (.. "Title: " title "\nAuthor: " author "\nModified: " modified "\nLicence: " licence "\n" link))
  (lg.setFont fonts.small-text)
  (lg.printf text 4 4 (- w 8) :left)
  (handle-mouse click over (.. text x y) (fn [] (love.system.openURL (.. "https://" link))))
  (lg.pop)
  )


{: draw-credit : get-mouse-info : button : click-text}
