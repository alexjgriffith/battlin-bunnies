(fn empty-level [name?]
  (local tile-size 64)
  (local {: editor : brush : level : creation-callbacks : preview-callbacks : ground-brushes} (require :editor-setup))
  (local {: atlas} (require :assets))
  (local l1 (level.create (or name? :l1)
                          (* 100 tile-size)
                          (* 100 tile-size)
                          {:x tile-size :y tile-size} :assets/sprites/atlas.png
                          creation-callbacks preview-callbacks))
  (-> l1
      (l1.add-layer :ground :tile ground-brushes :assets/sprites/atlas.png)
      (l1.add-layer :buttons :objects atlas.brushes)
      (l1.add-layer :objects :objects atlas.brushes))
  (l1.add l1 :objects (* 10 64) (* 10 64) :Player)
  (l1.add l1 :objects (* 14 64) (* 10 64) :Mike)
  (l1.add l1 :objects (* 20 64) (* 10 64) :Loaf)
  l1)

(fn load [name]
  (local {: editor : brush : level : creation-callbacks : preview-callbacks : ground-brushes} (require :editor-setup))
  (local {: atlas} (require :assets))
  (level.load-map-with-brushes name 
                  creation-callbacks preview-callbacks {:objects atlas.brushes :buttons atlas.brushes
                                                                     :ground ground-brushes}))

{: empty-level : load}
