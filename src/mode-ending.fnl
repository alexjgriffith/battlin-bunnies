(local lg love.graphics)
(local gamestate (require :lib.gamestate))
(local vink (require :lib.vink24))
(local {: fonts : atlas} (require :assets))
(local {: get-mouse-info : button : click-text} (require :ui))
(local fade (require :lib.fade))
(local {: fade-out-switch : fade-out-push } (require :transition))

{:enter (fn enter [obj]
          (fade.in))
 
 :draw (fn draw [obj message]
         (love.graphics.setBackgroundColor vink.purple-4)
         (lg.setColor vink.beige-1)
         (lg.setFont fonts.main-title)

         (lg.setFont fonts.text)
         (lg.printf "You've befriended all the bunnies on the island!" 0 (- 340 0) 1280 :center)
         (local mouse-info (get-mouse-info))
         (click-text mouse-info "Alexjgriffith" 20 664 1280 :left
                     (fn [] (love.system.openURL "https://alexjgriffith.itch.io")))
         (button mouse-info :Again? 500 (fade-out-switch :mode-world))
         (button mouse-info :Credits 590 (fade-out-push :mode-credits))
         (lg.draw fade.canvas))
 
 :update (fn update [obj dt set-mode])
 
 :keypressed (fn keypressed [key set-mode]
               (when (not (love.keyboard.isDown "lctrl" "rctrl" "capslock"))
                 (match key _ :nothing)))}
