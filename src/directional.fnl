;; https://www.gameaipro.com/GameAIPro2/GameAIPro2_Chapter18_Context_Steering_Behavior-Driven_Steering_at_the_Macro_Scale.pdf


(local target-weights [])
(local void-weights [])

(local dirs 16) ;;32

(local bias [] )
(for [i 1 dirs]
  (table.insert bias (+ 1 (* 0 (math.random)))))

(local cardinal-directions
       (fcollect [i 0 (- dirs 1)]
         (let [angle (* i (/ math.pi (/ dirs 2)))]
           {:x (math.cos angle) :y (math.sin angle)})))

;; Scalar projection
;; https://en.wikipedia.org/wiki/Dot_product
(fn dot-product [x1 y1 x2 y2]
  "for projection v2 should be normalized"
  (+ (* x1 x2) (* y1 y2)))

(fn euclidean-distance [x1 y1 x2 y2]
  (math.sqrt (+ (^ (- x1 x2) 2) (^ (- y1 y2) 2))))

(fn magnatude [x y]
  (math.sqrt (+ (^ x 2) (^ y 2))))

(fn cardinal-projection [x1 y1 x2 y2 i shapefun]
  (let [vx (- x2 x1)
        vy (- y2 y1)
        m (math.sqrt (+ (^ vx 2) (^ vy 2)))
        uvx (if (~= 0 m) (/ vx m) 0)
        uvy (if (~= 0 m) (/ vy m) 0)
        (uvxp uvyp) (shapefun uvx uvy (euclidean-distance
                                       x1 y1 x2 y2))
        {:x cx :y cy} (. cardinal-directions i)
        ]
    (dot-product uvxp uvyp cx cy)))

(fn array-min [array]
  (var min (. array 1))
  (for [i 2 (# array)]
    (when (< (. array i) min)
      (set min (. array i)))
    )
  min)

(fn index-max [array]
  (var min (. array 1))
  (var index 1)
  (for [i 2 (# array)]
    (when (> (. array i) min)
      (set min (. array i))
      (set index i)))
  index)

(fn all-zeros [array]
  (var ret true)
  (for [i 1 (# array)]
    (when (~= 0 (. array i )) (set ret false)))
  ret)

(fn update [x y target voids shapefn? shapemask? max-distance ]
  "
x & y numbers
target {:x :y}
voids [{:x :y}]
shapefun? (fn [value distance]) | nil
shapemask? (fn [value distance]) | nil
returns max index and target-weights
target-weights should just be used for visualization"
  (let [l (# cardinal-directions)
        {:x tx :y ty} target
        target-distance (euclidean-distance x y tx ty)
        shapefn (or shapefn? (fn [ux uy distance] (values ux uy)))
        shapemask (or shapemask? (fn [ux uy distance] (values ux uy)))]
    ;; clear target and mask vectors
    (for [i 1 l]
      (tset target-weights i (* (math.max 0 (cardinal-projection x y tx ty i shapefn))
                                (. bias i)))
      (tset void-weights i 0))
    (for [j 1 (# voids)]
      (let [{:x vx :y vy} (. voids j)]
        (for [i 1 l]
          (let [value (. void-weights i)
                distance (euclidean-distance x y vx vy)
                new-value (cardinal-projection x y vx vy i shapemask)
                nvp (* (^ (math.max 0 new-value) 4) 3
                       (/ (- max-distance  distance) max-distance))]
            (tset void-weights i (math.max value nvp))))))

    ;; mask
    (let [min (array-min void-weights)]
      (for [i 1 l]
        (when (> (. void-weights i) min)
          ;; (tset target-weights i 0)
          (let [tw (. target-weights i)
                vw (. void-weights i)]
            (tset target-weights i (math.max 0 (- tw vw))))
          )))
    (values (not (all-zeros target-weights)) (index-max target-weights) target-weights
            void-weights)
    ))


{: update}
