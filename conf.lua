love.conf = function(t)
   t.gammacorrect = false
   t.title, t.identity = "bunny-buddies", "Bunny Buddies"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 1280
   t.window.height = 720
   t.window.vsync = true
   t.window.display = 1
   t.version = "11.4"
end
