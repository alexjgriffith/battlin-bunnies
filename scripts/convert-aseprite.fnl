(local json (require :lib.json))
(local atlas-json (love.filesystem.read "art/atlas.json"))

(local parsed (json.decode atlas-json))


;; :SmallTree   {:bounds {:scale :tile :x 1 :y 11  :w 1 :h 2}
;;                 :data   {:cx 24 :cy 112 :cw 16 :ch 16}}
(local slices (. parsed :meta :slices))

(collect [_ slice (pairs slices)]
  (values slice.name {:bounds (. slice.keys 1 :bounds) :data {}})
  )

;;(local matlas (fennel.compile-string (love.filesystem.read :assets/sprites/atlas.fnl)))

(local matlas (require :assets/sprites/atlas))

(local slatlas (. matlas.slices))

(collect [key o (pairs slatlas)]
  (when (= o.bounds.scale :pixel)
    (set o.bounds.h (/ o.bounds.h 64))
    (set o.bounds.w (/ o.bounds.w 64))
    (set o.bounds.x (/ o.bounds.x 64))
    (set o.bounds.y (/ o.bounds.y 64))
    (set o.bounds.scale "tile")
    (values key o)
    )
  )
